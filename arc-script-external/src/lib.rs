use std::{
    fmt::Display,
    io::Write,
    path::{Path, PathBuf},
    process::{Command, Stdio},
};

use arc_core::{
    InitializationConfig, Script, ENV_CURRENT_DOCUMENT, ENV_CURRENT_TEXT, ENV_DOCUMENTS,
};

pub struct ExternalScript {
    command: PathBuf,
    config: InitializationConfig,
}

impl ExternalScript {
    pub fn new<P: AsRef<Path>>(config: InitializationConfig, command: P) -> Self {
        log::debug!(
            "Setting up an external script for the command: {}.",
            command.as_ref().display()
        );
        Self {
            config,
            command: command.as_ref().to_owned(),
        }
    }
}

#[derive(Debug)]
pub enum ExternalScriptError {
    Io(std::io::Error),
    Serialize(serde_json::Error),
    ScriptExitedFailure(i32),
}

impl Display for ExternalScriptError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(e) => write!(f, "spawing an external script failed: {e}")?,
            Self::Serialize(e) => write!(f, "failed to serialize the input: {e}")?,
            Self::ScriptExitedFailure(e) => write!(
                f,
                "the external script did not exit cleanly. Exit code: {e}"
            )?,
        }
        Ok(())
    }
}

impl std::error::Error for ExternalScriptError {}

impl From<std::io::Error> for ExternalScriptError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<ExternalScriptError> for arc_core::ScriptError {
    fn from(value: ExternalScriptError) -> Self {
        arc_core::ScriptError::ScriptError(Box::new(value))
    }
}

impl Script for ExternalScript {
    fn run(
        &self,
        document: arc_core::ScriptInput,
    ) -> Result<arc_core::ScriptOutput, arc_core::ScriptError> {
        log::debug!(
            "Calling external script command: {}.",
            self.command.display()
        );
        log::trace!("Building the command");
        let mut command = Command::new(self.config.script_path(&self.command));
        // Current dir
        log::trace!("Setting the current directory");
        command.current_dir(self.config.store_path());
        // Env variables
        log::trace!("Setting environmental variables.");

        let store_path = self.config.store_path();
        log::trace!("    - {} = {}", ENV_DOCUMENTS, store_path.display());
        command.env(ENV_DOCUMENTS, store_path);

        if let Some(doc) = &document.document {
            let current_document_path = self.config.document_path(doc.id, &doc.file_extension);
            log::trace!(
                "    - {} = {}",
                ENV_CURRENT_DOCUMENT,
                current_document_path.display()
            );
            command.env(ENV_CURRENT_DOCUMENT, current_document_path);
            let current_text_path = self.config.text_path(doc.id);
            log::trace!(
                "    - {} = {}",
                ENV_CURRENT_TEXT,
                current_text_path.display()
            );
            command.env(ENV_CURRENT_TEXT, current_text_path);
        }

        // Pipes
        log::trace!("Setting stdin pipes.");
        command.stdin(Stdio::piped());
        // command.stdout(Stdio::piped());

        log::debug!("Running external command: {:?}", command);
        let mut child = command.spawn().map_err(|e| {
            arc_core::ScriptError::ScriptError(Box::new(ExternalScriptError::from(e)))
        })?;
        // TODO: When is stdin not available?
        if let Some(stdin) = &mut child.stdin {
            stdin
                .write(
                    serde_json::to_string(&document)
                        .map_err(|e| {
                            arc_core::ScriptError::ScriptError(Box::new(
                                ExternalScriptError::Serialize(e),
                            ))
                        })?
                        .as_bytes(),
                )
                .map_err(|e| {
                    arc_core::ScriptError::ScriptError(Box::new(ExternalScriptError::Io(e)))
                })?;
        }
        let result = child.wait_with_output().map_err(|e| {
            arc_core::ScriptError::ScriptError(Box::new(ExternalScriptError::from(e)))
        })?;

        if !result.status.success() {
            log::error!(
                "The external command did not exit cleanly: Exit code {}.",
                result.status.code().unwrap_or(-1)
            );
            return Err(ExternalScriptError::ScriptExitedFailure(
                result.status.code().unwrap_or(-1),
            )
            .into());
        }

        Ok(())
    }
}
