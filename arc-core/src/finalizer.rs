use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
};

use mime::Mime;
use serde::{Deserialize, Serialize};

use crate::{DocumentId, Slug};

/// The input for a [Finalizer].
///
/// A [Finalizer] should use this data to finalize the document.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FinalizerInput {
    /// The [id][crate::Document::id] of the [Document][crate::Document].
    pub id: DocumentId,
    /// The [slug][crate::Document::slug] of the [Document][crate::Document].
    pub slug: Slug,
    /// The [file extension][crate::Document::file_extension] of the [Document][crate::Document].
    pub file_extension: String,
    /// The [mime_type][crate::Document::mime_type] of the [Document][crate::Document].
    #[serde(with = "mime_serde_shim")]
    pub mime_type: Mime,
    /// The [title][crate::Document::title] of the [Document][crate::Document].
    pub title: String,
    /// The [tags][crate::Document::tags] of the [Document][crate::Document].
    pub tags: HashSet<Slug>,
    /// The [data][crate::Document::data] of the [Document][crate::Document].
    pub data: HashMap<Slug, String>,
    /// The configuration for the [Finalizer].
    pub config: HashMap<String, String>,
}

/// The output for a [Finalizer].
///
/// This data will be used replace the old values of the document.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct FinalizerOutput {
    /// The [slug][crate::Document::slug] of the [Document][crate::Document].
    pub slug: Slug,
    /// The [title][crate::Document::title] of the [Document][crate::Document].
    pub title: String,
    /// The [tags][crate::Document::tags] of the [Document][crate::Document].
    pub tags: HashSet<Slug>,
    /// The [data][crate::Document::data] of the [Document][crate::Document].
    pub data: HashMap<Slug, String>,
}

/// The errors that may be returned by a [Finalizer].
#[derive(Debug)]
pub enum FinalizerError {
    /// A finaliizer-specific issue occured.
    FinalizerError(Box<dyn std::error::Error>),
}

impl Display for FinalizerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::FinalizerError(e) => write!(f, "the finalizer failed: {e}")?,
        }
        Ok(())
    }
}

impl std::error::Error for FinalizerError {}

/// A [Finalizer] finalizes a [Document][crate::Document].
pub trait Finalizer {
    /// Give the [FinalizerOutput] based on the [FinalizerInput].
    ///
    /// Note that the finalizer can use the stored document and also (if provided by the extractor) the textual representation of the document. Furthermore, the changes proposed by an output of the finalizer will overwrite the data of the document.
    fn finalize(&self, document: FinalizerInput) -> Result<FinalizerOutput, FinalizerError>;
}
