/// The environmental variable used by [InitializationConfig][crate::InitializationConfig] to determine the location of the store.
pub const ENV_DOCUMENTS: &str = "ARC_DOCUMENTS";
/// The environmental variable used by [InitializationConfig][crate::InitializationConfig] to determine the location of the configuration file. If not given, the location will be inferred by the document path.
pub const ENV_CONFIG: &str = "ARC_CONFIG";
/// The environmental variable used to indicate the location of the current document.
pub const ENV_CURRENT_DOCUMENT: &str = "ARC_CURRENT_DOCUMENT";
/// The environmental variable used to indicate the location of the text of the current document.
pub const ENV_CURRENT_TEXT: &str = "ARC_CURRENT_TEXT";
