use crate::Document;

/// The trait for allowing full-text search in a document.
pub trait SearchProvider {
    /// Specifies if a document contains the text with the given argument.
    ///
    /// If this function returns `true`, this document will be allowed to pass into the next stage. Otherwise, this document will not be shown in the search results.
    /// TODO: Maybe allow errors?
    fn contains(&self, document: &Document, text: &str) -> bool;
}
