use crate::Document;

pub trait SearchCustomFilter {
    /// Specifies if a document matches the custom filter with the given argument.
    ///
    /// If this function returns `true`, this document will be allowed to pass into the next stage. Otherwise, this document will not be shown in the search results.
    /// The argument is the one the user specified for the custom filter.
    /// TODO: Maybe allow errors?
    fn matches(&self, document: &Document, arg: Option<&str>) -> bool;
}
