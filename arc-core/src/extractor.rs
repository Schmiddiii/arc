use std::collections::HashMap;
use std::fmt::Display;

use mime::Mime;
use serde::{Deserialize, Serialize};

use crate::{DocumentId, Slug};

/// The input given to an [Extractor].
///
/// The [Extractor] will use this data to extract the data.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ExtractionInput {
    /// The [id][crate::Document::id] of the [Document][crate::Document].
    pub id: DocumentId,
    /// The [slug][crate::Document::slug] of the [Document][crate::Document].
    pub slug: Slug,
    /// The [file extension][crate::Document::file_extension] of the [Document][crate::Document].
    pub file_extension: String,
    /// The [mime type][crate::Document::mime_type] of the [Document][crate::Document].
    #[serde(with = "mime_serde_shim")]
    pub mime_type: Mime,
    /// The configuration of the [Extractor].
    pub config: HashMap<String, String>,
}

/// The output the [Extractor] may give.
///
/// Note that the extracted content is stored in a separate text document and is not given in the extraction output.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ExtractionOutput {
    /// The [title][crate::Document::title] of the [Document][crate::Document].
    pub title: String,
}

/// The errors that may occure for an [Extractor].
#[derive(Debug)]
pub enum ExtractionError {
    /// The mime type of the document given does not correspond to the mime type the [Extractor] is made for.
    ///
    /// Note that the first [Mime] corresponds to the input mime type of the document, the second one corresponds to the expected mime type.
    InvalidMimeType(Mime, Mime),
    /// An [Extractor]-specific error.
    ExtractorError(Box<dyn std::error::Error>),
}

impl Display for ExtractionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidMimeType(got, expected) => write!(
                f,
                "extractor got an invalid mime type. It expected {expected}, but got {got}"
            )?,
            Self::ExtractorError(e) => write!(f, "the extractor failed: {e}")?,
        }
        Ok(())
    }
}

impl std::error::Error for ExtractionError {}

/// Extract data from a [Document][crate::Document].
///
/// This consumes an [ExtractionInput] and produces an [ExtractionOutput]. Additionally, the content of the document should be stored in a separate file.
pub trait Extractor {
    /// Extract data from a document specified in the [ExtractionInput], giving the [ExtractionOutput] and the additional file that contains the content.
    fn extract(&self, document: ExtractionInput) -> Result<ExtractionOutput, ExtractionError>;
}
