use std::collections::{HashMap, HashSet};

use crate::Slug;

const TOKEN_TAG_DATA: char = '#';
const TOKEN_CUSTOM_FILTER: char = '@';
const TOKEN_SPLIT: char = ':';
const TOKEN_QUOTE: char = '"';
const TITLE_TEXT_SEPARATOR: &str = " - ";

#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct SearchQuery {
    pub title: String,
    pub text: String,
    pub tags: HashSet<Slug>,
    pub data: HashMap<Slug, String>,
    pub custom_filters: HashMap<Slug, Option<String>>,
}

fn parse_quoted(q: &str, allow_splits: bool) -> (&str, &str) {
    if q.starts_with(TOKEN_QUOTE) {
        q[1..].split_once(TOKEN_QUOTE).unwrap_or((&q[1..], ""))
    } else {
        let split = if allow_splits {
            q.find(char::is_whitespace)
                .unwrap_or(q.len())
                .min(q.find(TOKEN_SPLIT).unwrap_or(q.len()))
        } else {
            q.find(char::is_whitespace).unwrap_or(q.len())
        };
        q.split_at(split)
    }
}

fn parse_key_value(q: &str) -> ((&str, Option<&str>), &str) {
    let (key, rest) = parse_quoted(q, true);
    if rest.starts_with(TOKEN_SPLIT) {
        let (value, rest) = parse_quoted(&rest[1..], false);
        ((key, Some(value)), rest)
    } else {
        ((key, None), rest)
    }
}

impl SearchQuery {
    pub fn parse<S: AsRef<str>>(q: S) -> Self {
        let mut query = Self::default();
        let mut q = q.as_ref();

        while !q.is_empty() {
            let idx_tag_data = q.find(TOKEN_TAG_DATA).unwrap_or(q.len());
            let idx_custom_filter = q.find(TOKEN_CUSTOM_FILTER).unwrap_or(q.len());

            let idx_min = idx_tag_data.min(idx_custom_filter);

            // There is nothing at the start. Split at the next interesting occurence.
            if idx_min > 0 {
                let (text, rest) = q.split_at(idx_min);
                query.title += text.trim();
                q = rest.trim();
            } else if idx_tag_data == 0 {
                q = &q[1..];
                let ((key, value), rest) = parse_key_value(q);
                if let Some(value) = value {
                    query.data.insert(key.into(), value.to_string());
                } else {
                    query.tags.insert(key.into());
                }
                q = rest;
            } else if idx_custom_filter == 0 {
                q = &q[1..];
                let ((key, value), rest) = parse_key_value(q);
                query
                    .custom_filters
                    .insert(key.into(), value.map(|s| s.to_string()));
                q = rest;
            } else {
                panic!("Cannot happen that idx_min == 0 but idx_tag_data != 0 and idx_custom_filter != 0");
            }
        }

        // When it starts with the separator, everything is text.
        if let Some(text) = query.title.strip_prefix(TITLE_TEXT_SEPARATOR.trim()) {
            query.text = text.trim_start().to_string();
            query.title = String::new();
        }

        // When it ends with the separator, everything is title.
        if let Some(title) = query.title.strip_suffix(TITLE_TEXT_SEPARATOR.trim()) {
            query.title = title.trim().to_string();
        }

        if let Some((title, text)) = query
            .title
            .split_once(TITLE_TEXT_SEPARATOR)
            .map(|(ti, te)| (ti.to_string(), te.to_string()))
        {
            query.title = title;
            query.text = text;
        }

        query
    }
}

#[cfg(test)]
mod test {
    use super::*;

    fn check<S: AsRef<str>>(query: S, expected: &SearchQuery) {
        assert_eq!(&SearchQuery::parse(query), expected)
    }

    #[test]
    fn search_text_only() {
        check(
            "This is only some text",
            &SearchQuery {
                title: "This is only some text".to_owned(),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_tag_only() {
        check(
            "#some-tag",
            &SearchQuery {
                tags: HashSet::from(["some-tag".into()]),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_data_only() {
        check(
            "#some-key:value",
            &SearchQuery {
                data: HashMap::from([("some-key".into(), "value".to_owned())]),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_custom_no_arg() {
        check(
            "@custom",
            &SearchQuery {
                custom_filters: HashMap::from([("custom".into(), None)]),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_custom_arg() {
        check(
            "@custom:arg",
            &SearchQuery {
                custom_filters: HashMap::from([("custom".into(), Some("arg".to_owned()))]),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_text_tag_mix() {
        check(
            "Some Text #some-tag",
            &SearchQuery {
                title: "Some Text".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from(["some-tag".into()]),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_documentation_no_title() {
        check(
            "My top secret information #top-secret #classification-level:5 @later:yesterday",
            &SearchQuery {
                title: "My top secret information".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from(["top-secret".into()]),
                data: HashMap::from([("classification-level".into(), "5".to_owned())]),
                custom_filters: HashMap::from([("later".into(), Some("yesterday".to_owned()))]),
            },
        );
    }

    #[test]
    fn search_quoted_tag() {
        check(
            r#"This is #"So Cool""#,
            &SearchQuery {
                title: "This is".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from(["So Cool".into()]),
                data: HashMap::from([]),
                custom_filters: HashMap::from([]),
            },
        );
    }

    #[test]
    fn search_quoted_data() {
        check(
            r#"This is #"So Cool":"Data Value""#,
            &SearchQuery {
                title: "This is".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from([]),
                data: HashMap::from([("So Cool".into(), "Data Value".into())]),
                custom_filters: HashMap::from([]),
            },
        );
    }

    #[test]
    fn search_quoted_custom() {
        check(
            r#"This is @"So Cool":"Data Value""#,
            &SearchQuery {
                title: "This is".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from([]),
                data: HashMap::from([]),
                custom_filters: HashMap::from([("So Cool".into(), Some("Data Value".to_owned()))]),
            },
        );
    }

    #[test]
    fn search_quoted_in_text() {
        check(
            r#"This is "John Cena""#,
            &SearchQuery {
                title: r#"This is "John Cena""#.to_owned(),
                text: "".to_owned(),
                tags: HashSet::from([]),
                data: HashMap::from([]),
                custom_filters: HashMap::from([]),
            },
        );
    }

    #[test]
    fn search_tag_no_text() {
        check(
            r#"This is a random #"#,
            &SearchQuery {
                title: "This is a random".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from(["".into()]),
                data: HashMap::from([]),
                custom_filters: HashMap::from([]),
            },
        );
    }

    #[test]
    fn search_data_no_text() {
        check(
            r#"This is a random #:"#,
            &SearchQuery {
                title: "This is a random".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from([]),
                data: HashMap::from([("".into(), "".to_string())]),
                custom_filters: HashMap::from([]),
            },
        );
    }

    #[test]
    fn search_custom_no_text() {
        check(
            r#"This is a random @"#,
            &SearchQuery {
                title: "This is a random".into(),
                text: "".to_owned(),
                tags: HashSet::from([]),
                data: HashMap::from([]),
                custom_filters: HashMap::from([("".into(), None)]),
            },
        );
    }

    #[test]
    fn search_custom_no_text_no_data() {
        check(
            r#"This is a random @:"#,
            &SearchQuery {
                title: "This is a random".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from([]),
                data: HashMap::from([]),
                custom_filters: HashMap::from([("".into(), Some("".to_string()))]),
            },
        );
    }

    #[test]
    fn search_data_two_colons() {
        check(
            "#key:data1:data2",
            &SearchQuery {
                title: "".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from([]),
                data: HashMap::from([("key".into(), "data1:data2".to_string())]),
                custom_filters: HashMap::from([]),
            },
        );
    }

    #[test]
    fn search_data_colon_in_quote() {
        check(
            r#"#"key1:key2":data1"#,
            &SearchQuery {
                title: "".to_owned(),
                text: "".to_owned(),
                tags: HashSet::from([]),
                data: HashMap::from([("key1:key2".into(), "data1".to_string())]),
                custom_filters: HashMap::from([]),
            },
        );
    }

    #[test]
    fn search_empty_text() {
        check(
            "Hello - ",
            &SearchQuery {
                title: "Hello".to_owned(),
                text: "".to_owned(),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_only_dash_empty() {
        check(
            " - ",
            &SearchQuery {
                title: "".to_owned(),
                text: "".to_owned(),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_empty_title() {
        check(
            " - Hello",
            &SearchQuery {
                title: "".to_owned(),
                text: "Hello".to_owned(),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_empty_title_multiple_separators() {
        check(
            " - World - and everything else",
            &SearchQuery {
                title: "".to_owned(),
                text: "World - and everything else".to_owned(),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_text_and_title() {
        check(
            "Hello - World",
            &SearchQuery {
                title: "Hello".to_owned(),
                text: "World".to_owned(),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_text_and_title_multiple_separators() {
        check(
            "Hello - World - and everything else",
            &SearchQuery {
                title: "Hello".to_owned(),
                text: "World - and everything else".to_owned(),
                ..Default::default()
            },
        );
    }

    #[test]
    fn search_documentation() {
        check(
            "Secrets - My top secret information #top-secret #classification-level:5 @later:yesterday",
            &SearchQuery {
                title: "Secrets".to_owned(),
                text: "My top secret information".to_owned(),
                tags: HashSet::from(["top-secret".into()]),
                data: HashMap::from([("classification-level".into(), "5".to_owned())]),
                custom_filters: HashMap::from([("later".into(), Some("yesterday".to_owned()))]),
            },
        );
    }
}
