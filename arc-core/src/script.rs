use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
};

use mime::Mime;
use serde::{Deserialize, Serialize};

use crate::{DocumentId, Slug};

/// The input for a [Script].
///
/// A [Script] uses this data to run.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ScriptInput {
    /// The optional document the script gets.
    #[serde(flatten)]
    pub document: Option<ScriptInputDocument>,
    /// The configuration for the [Script].
    pub config: HashMap<String, String>,
}

/// The document input for a [Script].
///
/// A [Script] uses this data to run.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct ScriptInputDocument {
    /// The [id][crate::Document::id] of the [Document][crate::Document].
    pub id: DocumentId,
    /// The [slug][crate::Document::slug] of the [Document][crate::Document].
    pub slug: Slug,
    /// The [file extension][crate::Document::file_extension] of the [Document][crate::Document].
    pub file_extension: String,
    /// The [mime_type][crate::Document::mime_type] of the [Document][crate::Document].
    #[serde(with = "mime_serde_shim")]
    pub mime_type: Mime,
    /// The [title][crate::Document::title] of the [Document][crate::Document].
    pub title: String,
    /// The [tags][crate::Document::tags] of the [Document][crate::Document].
    pub tags: HashSet<Slug>,
    /// The [data][crate::Document::data] of the [Document][crate::Document].
    pub data: HashMap<Slug, String>,
}

/// The output for a [Script].
///
/// Currently, scripts do not have an output.
pub type ScriptOutput = ();

/// The errors that may be returned by a [Script].
#[derive(Debug)]
pub enum ScriptError {
    /// A finaliizer-specific issue occured.
    ScriptError(Box<dyn std::error::Error>),
}

impl Display for ScriptError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ScriptError(e) => write!(f, "the script failed: {e}")?,
        }
        Ok(())
    }
}

impl std::error::Error for ScriptError {}

/// A [Script] runs on a [Document][crate::Document], producing nothing.
pub trait Script {
    /// Give the [ScriptOutput] based on the [ScriptInput].
    ///
    /// Note that currently scripts may not produce a return value.
    fn run(&self, document: ScriptInput) -> Result<ScriptOutput, ScriptError>;
}
