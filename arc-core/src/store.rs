use std::collections::{HashMap, HashSet};

use mime::Mime;

use crate::{Document, DocumentId, InitializationConfig, Slug};

/// A general trait on how to store [Documents][Document]
pub trait Store {
    /// The error that may be returned by the store.
    type Error: std::error::Error;

    /// Create the store from an [InitializationConfig] and set everything up.
    fn from_initialization_config(config: InitializationConfig) -> Result<Self, Self::Error>
    where
        Self: Sized;

    /// Create a [Document] just from the slug, giving back the id of the created document.
    ///
    /// The id should be the minimum ID that is currently unused (the first id is 1). All the values except the id and slug should be set to their default values.
    fn create_from_slug(&self, slug: &Slug) -> Result<DocumentId, Self::Error>;

    /// Create a [Document] just from the minimum, giving back the created document.
    ///
    /// The mimimum information consists of:
    /// - The [slug][Document::slug]
    /// - The [file_extension][Document::file_extension]
    /// - The [mime_type][Document::mime_type]
    ///
    /// The id should be the minimum ID that is currently unused (the first id is 1). All the values except the `id`, `slug`, `file_extension` and `mime_type` should be set to their default values.
    /// Note that a default implementation is given, but this should be overwritten.
    fn create_from_basics<S: AsRef<str>>(
        &self,
        slug: &Slug,
        file_extension: S,
        mime_type: Mime,
    ) -> Result<Document, Self::Error> {
        let id = self.create_from_slug(slug)?;
        let doc = Document {
            id,
            slug: slug.clone(),
            file_extension: file_extension.as_ref().to_string(),
            mime_type,
            title: Default::default(),
            tags: Default::default(),
            data: Default::default(),
        };
        self.set(&doc)?;
        Ok(self.get(id)?.expect("The created document to exist"))
    }

    /// Set a [Document].
    ///
    /// This should use the [id][Document::id] of the document to identify the document in the store that should be replaced.
    fn set(&self, document: &Document) -> Result<(), Self::Error>;

    /// Get a [Document] by its [DocumentId].
    fn get(&self, id: DocumentId) -> Result<Option<Document>, Self::Error>;

    /// Get the latest [Document].
    ///
    /// Note that there is a default implementation, but for performance reasons an implementation on the store is recommended.
    fn get_latest(&self) -> Result<Option<Document>, Self::Error> {
        log::trace!("Getting latest document.");
        let all = self.get_all()?;
        Ok(all.into_iter().last())
    }

    /// Get all the [Documents][Document] with a given [Slug]..
    ///
    /// Note that there is a default implementation, but for performance reasons an implementation on the store is recommended.
    fn get_by_slug(&self, slug: &Slug) -> Result<Vec<Document>, Self::Error> {
        log::trace!("Getting document by slug {slug}.");
        let all = self.get_all()?;
        Ok(all.into_iter().filter(|d| &d.slug == slug).collect())
    }

    /// Get all the [DocumentIds][DocumentId] in the [Store].
    ///
    /// The [DocumentIds][DocumentId] should be ordered in increasing order.
    /// Note that this should pretty much return `vec![1, 2, 3, ..., max_document_id]`, but potentially later there will be a way to delete documents.
    fn get_all_ids(&self) -> Result<Vec<DocumentId>, Self::Error>;

    /// Get all the [Documents][Document] in the [Store].
    ///
    /// Note that a default implementation is given, but for performance reasons, an implementation on the store is recommended.
    fn get_all(&self) -> Result<Vec<Document>, Self::Error> {
        log::trace!("Getting all documents.");
        let ids = self.get_all_ids()?;
        Ok(ids
            .into_iter()
            .map(|id| self.get(id))
            .collect::<Result<Vec<Option<Document>>, Self::Error>>()?
            .into_iter()
            .flatten()
            .collect::<Vec<Document>>())
    }

    /// Get all [Documents][Document] in the [Store] has all the tags and data points.
    ///
    /// Note that a default implementation is given, but for performance reasons, an implementation on the store is recommended.
    fn search<S: AsRef<str>>(
        &self,
        title: S,
        tags: &HashSet<Slug>,
        data: &HashMap<Slug, String>,
    ) -> Result<Vec<Document>, Self::Error> {
        Ok(self
            .get_all()?
            .into_iter()
            .filter(|d| {
                let document_title = d.title.to_lowercase();
                tags.is_subset(&d.tags)
                    && data
                        .iter()
                        .all(|(k, v)| d.data.get(k).is_some_and(|d| d == v))
                    && title
                        .as_ref()
                        .to_lowercase()
                        .split_whitespace()
                        .all(|w| document_title.contains(w))
            })
            .collect())
    }
}
