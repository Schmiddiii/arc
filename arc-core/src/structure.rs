use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
};

use arc_util::sluggify;
use mime::Mime;
use serde::{Deserialize, Serialize};

/// The identification type of a [Document].
pub type DocumentId = u64;

/// A [Document] represents a real document stored inside the application.
///
/// Note that a document does not store the actual contents of the document, just the abstract representation.
#[derive(Serialize, Deserialize, Clone, Debug, PartialEq, Eq)]
pub struct Document {
    /// The unique [DocumentId] of the document in the [Store][crate::Store].
    pub id: DocumentId,
    /// The slug of the [Document].
    ///
    /// Note that a slug may not be globally unique, but it is (for an user) easier to refer to a slug than to an id. The slug should be in a "sluggified format", see [sluggify][arc_util::sluggify].
    pub slug: Slug,
    /// The file extension of the [Document].
    #[serde(rename = "file-extension")]
    pub file_extension: String,
    /// The mime type of the [Document].
    #[serde(with = "mime_serde_shim")]
    #[serde(rename = "mime-type")]
    pub mime_type: Mime,
    /// The
    pub title: String,
    /// The tags assign to the [Document].
    ///
    /// Note that tags are not stored multiple times per [Document].
    pub tags: HashSet<Slug>,
    /// The data assign to the [Document].
    ///
    /// Note that one key may refer to at most one value per [Document].
    pub data: HashMap<Slug, String>,
}

/// The wrapper type around a [String] that makes sure that the included [String] is [sluggified][arc_util::sluggify].
#[derive(Debug, Clone, Serialize, PartialEq, Eq, Hash)]
pub struct Slug(String);

impl<'de> Deserialize<'de> for Slug {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        Ok(String::deserialize(deserializer)?.into())
    }
}

impl Slug {
    pub fn new<S: AsRef<str>>(s: S) -> Self {
        s.as_ref().to_owned().into()
    }
}

impl Display for Slug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl AsRef<str> for Slug {
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl From<String> for Slug {
    fn from(value: String) -> Self {
        Self(sluggify(value))
    }
}

impl From<&str> for Slug {
    fn from(value: &str) -> Self {
        value.to_owned().into()
    }
}

impl From<Slug> for String {
    fn from(value: Slug) -> Self {
        value.0
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn document_deserialize() -> Result<(), Box<dyn std::error::Error>> {
        let d = r#"
            {
                "id": 123,
                "slug": "some-slug",
                "file-extension": "pdf",
                "mime-type": "application/pdf",
                "title": "Automatically extracted Title",
                "tags": ["some-tag", "another-tag"],
                "data": {
                    "key1": "value1",
                    "key2": "value2"
                }
            }
        "#;
        let document: Document = serde_json::from_str(d)?;
        assert_eq!(
            Document {
                id: 123,
                slug: "some-slug".into(),
                file_extension: "pdf".into(),
                mime_type: mime::APPLICATION_PDF,
                title: "Automatically extracted Title".into(),
                tags: vec!["some-tag".into(), "another-tag".into()]
                    .into_iter()
                    .collect(),
                data: HashMap::from([
                    ("key1".into(), "value1".to_owned()),
                    ("key2".into(), "value2".to_owned())
                ]),
            },
            document
        );
        Ok(())
    }

    #[test]
    fn document_deserialize_malformed_slug() -> Result<(), Box<dyn std::error::Error>> {
        let d = r#"
            {
                "id": 123,
                "slug": "This is not in Slug form",
                "file-extension": "pdf",
                "mime-type": "application/pdf",
                "title": "Automatically extracted Title",
                "tags": ["some-tag", "another-tag"],
                "data": {
                    "key1": "value1",
                    "key2": "value2"
                }
            }
        "#;
        let document: Document = serde_json::from_str(d)?;
        assert_eq!(
            Document {
                id: 123,
                slug: "this-is-not-in-slug-form".into(),
                file_extension: "pdf".into(),
                mime_type: mime::APPLICATION_PDF,
                title: "Automatically extracted Title".into(),
                tags: vec!["some-tag".into(), "another-tag".into()]
                    .into_iter()
                    .collect(),
                data: HashMap::from([
                    ("key1".into(), "value1".to_owned()),
                    ("key2".into(), "value2".to_owned())
                ]),
            },
            document
        );
        Ok(())
    }
}
