use crate::{Document, SearchQuery};

/// A general trait on how the interaction with the user happens.
pub trait UserInteraction {
    type Error: std::error::Error;
    /// Display the contents of a document.
    ///
    /// This should e.g. open a program the user chose to view the file.
    fn open(&self, document: &Document) -> Result<(), Self::Error>;
    /// Show a document to the user.
    ///
    /// This should display all the information (slug, title, tags, data, ...) to the user but not actually display the file.
    fn show(&self, document: &Document) -> Result<(), Self::Error>;
    /// Edit a document, returning the updated document.
    ///
    /// The user may not edit the following fields:
    /// - ID.
    /// - File extension.
    /// - Mime type.
    fn edit(&self, document: Document) -> Result<Document, Self::Error>;
    /// Show multiple documents.
    ///
    /// Should be in a more compact format compared to [UserInteraction::show].
    fn show_all(&self, documents: &[Document]) -> Result<(), Self::Error>;
    /// Present the user a search to select a document.
    ///
    /// You should regularly call the `searcher` function, which returns all the matching documents given the search. The search process is therefore as follows:
    ///
    /// - The user modifies the search query.
    /// - The user interaction contructs the query and uses the `searcher` to get the list of matching documents.
    /// - Display the documents.
    /// - Repeat from the start as often as the user wants.
    /// - The user chooses a document from the list.
    /// - Return this document from the function.
    fn search<F: Fn(SearchQuery) -> Vec<Document>>(
        &self,
        searcher: F,
    ) -> Result<Option<Document>, Self::Error>;
}
