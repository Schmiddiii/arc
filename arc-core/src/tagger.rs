use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
};

use mime::Mime;
use serde::{Deserialize, Serialize};

use crate::{DocumentId, Slug};

/// The input for a [Tagger].
///
/// A [Tagger] should use this data to tag a document with tags.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TaggerInput {
    /// The [id][crate::Document::id] of the [Document][crate::Document].
    pub id: DocumentId,
    /// The [slug][crate::Document::slug] of the [Document][crate::Document].
    pub slug: Slug,
    /// The [file extension][crate::Document::file_extension] of the [Document][crate::Document].
    pub file_extension: String,
    /// The [mime_type][crate::Document::mime_type] of the [Document][crate::Document].
    #[serde(with = "mime_serde_shim")]
    pub mime_type: Mime,
    /// The [title][crate::Document::title] of the [Document][crate::Document].
    pub title: String,
    /// The configuration for the [Tagger].
    pub config: HashMap<String, String>,
}

/// The output for a [Tagger].
///
/// This data will be used to populate the fields of a document.
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct TaggerOutput {
    /// The [tags][crate::Document::tags] of the [Document][crate::Document].
    pub tags: HashSet<Slug>,
    /// The [data][crate::Document::data] of the [Document][crate::Document].
    pub data: HashMap<Slug, String>,
}

/// The errors that may be returned by a [Tagger].
#[derive(Debug)]
pub enum TaggerError {
    /// The mime type of the document given does not correspond to the mime type the [Tagger] is made for.
    ///
    /// Note that the first [Mime] corresponds to the input mime type of the document, the second one corresponds to the expected mime type.
    InvalidMimeType(Mime, Mime),
    /// A tagger-specific issue occured.
    TaggerError(Box<dyn std::error::Error>),
}

impl Display for TaggerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidMimeType(got, expected) => write!(
                f,
                "tagger got an invalid mime type. It expected {expected}, but got {got}"
            )?,
            Self::TaggerError(e) => write!(f, "the tagger failed: {e}")?,
        }
        Ok(())
    }
}

impl std::error::Error for TaggerError {}

/// A [Tagger] adds tags and data to a [Document][crate::Document].
pub trait Tagger {
    /// Give the [TaggerOutput] based on the [TaggerInput].
    ///
    /// Note that the tagger can use the stored document and also (if provided by the extractor) the textual representation of the document.
    fn tag(&self, document: TaggerInput) -> Result<TaggerOutput, TaggerError>;
}
