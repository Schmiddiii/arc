use std::path::{Path, PathBuf};

use crate::{DocumentId, Slug, ENV_CONFIG, ENV_DOCUMENTS};

fn absolute_or_join_with<P: AsRef<Path>>(path: P, join_with: impl Fn() -> PathBuf) -> PathBuf {
    let path = path.as_ref();
    if path.is_absolute() {
        path.to_owned()
    } else {
        join_with().join(path)
    }
}

/// The configuration used to specify the path of where all the data is stored.
#[derive(Clone, Debug)]
pub struct InitializationConfig {
    store_path: PathBuf,
    config_path: Option<PathBuf>,
}

impl InitializationConfig {
    /// Create a new [InitializationConfig] given the path to the store.
    pub fn new<P: AsRef<Path>>(store_path: P) -> Self {
        Self::new_with_config_path(store_path, None)
    }

    /// Create a new [InitializationConfig] given the path to the store.
    fn new_with_config_path<P: AsRef<Path>>(store_path: P, config_path: Option<P>) -> Self {
        log::debug!(
            "Creating new InitializationConfig for path: {}. and config {:?}",
            store_path.as_ref().display(),
            config_path.as_ref().map(|p| p.as_ref().display())
        );
        let store_path = store_path.as_ref();
        let store_path = if store_path.is_relative() {
            std::env::current_dir()
                .map(|d| d.join(store_path))
                .unwrap_or(store_path.to_owned())
        } else {
            store_path.to_owned()
        };

        let s = Self {
            store_path,
            config_path: config_path.map(|p| p.as_ref().to_owned()),
        };
        log::info!("Set up the initial config:");
        log::info!("    - Store path: {}", s.store_path().display());
        log::info!("    - .arc path: {}", s.dot_arc_path().display());
        log::info!("    - Documents path: {}", s.documents_path().display());
        log::info!("    - Texts path: {}", s.texts_path().display());
        log::info!("    - Extractors path: {}", s.extractors_path().display());
        log::info!("    - Taggers path: {}", s.taggers_path().display());
        log::info!("    - Finalizers path: {}", s.finalizers_path().display());
        log::info!("    - Scripts path: {}", s.scripts_path().display());
        s
    }

    /// Try to create a new [InitializationConfig] from the environmental variable specified in [ENV_DOCUMENTS].
    ///
    /// Returns [None][Option::None] if the environmental variable was not given.
    pub fn from_env() -> Option<Self> {
        log::debug!("Creating new InitializationConfig from environmental variable.");
        let store_path = std::env::var(ENV_DOCUMENTS).ok()?;
        let config_path = std::env::var(ENV_CONFIG).ok();
        Some(Self::new_with_config_path(store_path, config_path))
    }

    /// Set up the directory structure if that did not yet happen.
    pub fn setup_directory_structure(&self) -> Result<(), std::io::Error> {
        log::debug!("Setting up the directory structure.");
        arc_util::create_all_dir_if_not_exists(&[
            self.dot_arc_path(),
            self.documents_path(),
            self.texts_path(),
            self.extractors_path(),
            self.taggers_path(),
            self.finalizers_path(),
            self.scripts_path(),
        ])?;

        Ok(())
    }

    /// Give the root path of the store.
    pub fn store_path(&self) -> PathBuf {
        log::trace!("Querying the store path.");
        self.store_path.clone()
    }

    /// Give the path of the `.arc` subdirectory of the [store_path][Self::store_path].
    pub fn dot_arc_path(&self) -> PathBuf {
        log::trace!("Querying the .arc path.");
        self.store_path.join(".arc")
    }

    /// Give the path of the `documents` subdirectory of the [dot_arc_path][Self::dot_arc_path].
    pub fn documents_path(&self) -> PathBuf {
        log::trace!("Querying the documents path.");
        self.dot_arc_path().join("documents")
    }

    /// Give the path of where the document is stored based on the given parameters.
    pub fn document_path<S: std::fmt::Display>(
        &self,
        id: DocumentId,
        file_extension: S,
    ) -> PathBuf {
        log::trace!(
            "Querying the document path for document with id {id} and extension {file_extension}."
        );
        self.documents_path().join(format!("{id}.{file_extension}"))
    }

    /// Give the path of the `texts` subdirectory of the [dot_arc_path][Self::dot_arc_path].
    pub fn texts_path(&self) -> PathBuf {
        log::trace!("Querying the texts path.");
        self.dot_arc_path().join("texts")
    }

    /// Give the path of where the document text is stored based on the given parameters.
    pub fn text_path(&self, id: DocumentId) -> PathBuf {
        log::trace!("Querying the text path for document with id {id}.");
        self.texts_path().join(format!("{id}.txt"))
    }

    /// Give the path of the `extractors` subdirectory of the [dot_arc_path][Self::dot_arc_path].
    pub fn extractors_path(&self) -> PathBuf {
        log::trace!("Querying the extractors path.");
        self.dot_arc_path().join("extractors")
    }

    /// Give the path of where an extractor is located based on the given parameters.
    pub fn extractor_path<P: AsRef<Path>>(&self, extractor: P) -> PathBuf {
        log::trace!(
            "Querying the path for extractor: {}.",
            extractor.as_ref().display()
        );
        absolute_or_join_with(extractor, || self.extractors_path())
    }

    /// Give the path of the `taggers` subdirectory of the [dot_arc_path][Self::dot_arc_path].
    pub fn taggers_path(&self) -> PathBuf {
        log::trace!("Querying the taggers path.");
        self.dot_arc_path().join("taggers")
    }

    /// Give the path of where an tagger is located based on the given parameters.
    pub fn tagger_path<P: AsRef<Path>>(&self, tagger: P) -> PathBuf {
        log::trace!(
            "Querying the path for tagger: {}.",
            tagger.as_ref().display()
        );
        absolute_or_join_with(tagger, || self.taggers_path())
    }

    /// Give the path of the `finalizers` subdirectory of the [dot_arc_path][Self::dot_arc_path].
    pub fn finalizers_path(&self) -> PathBuf {
        log::trace!("Querying the finalizers path.");
        self.dot_arc_path().join("finalizers")
    }

    /// Give the path of where an finalizer is located based on the given parameters.
    pub fn finalizer_path<P: AsRef<Path>>(&self, finalizer: P) -> PathBuf {
        log::trace!(
            "Querying the path for finalizer: {}.",
            finalizer.as_ref().display()
        );
        absolute_or_join_with(finalizer, || self.finalizers_path())
    }

    /// Give the path of the `scripts` subdirectory of the [dot_arc_path][Self::dot_arc_path].
    pub fn scripts_path(&self) -> PathBuf {
        log::trace!("Querying the scripts path.");
        self.dot_arc_path().join("scripts")
    }

    /// Give the path of where a script is located based on the given parameters.
    pub fn script_path<P: AsRef<Path>>(&self, script: P) -> PathBuf {
        log::trace!(
            "Querying the path for script: {}.",
            script.as_ref().display()
        );
        absolute_or_join_with(script, || self.scripts_path())
    }

    /// Give the path of the configuration file.
    pub fn config_path(&self) -> PathBuf {
        log::trace!("Querying the config path.");
        self.config_path
            .clone()
            .unwrap_or_else(|| self.dot_arc_path().join("config.toml"))
    }

    pub fn extracted_document_path<S: std::fmt::Display>(
        &self,
        slug: Slug,
        file_extension: S,
    ) -> PathBuf {
        log::trace!(
            "Querying the extracted document path for document with slug {slug} and extension {file_extension}."
        );
        self.store_path().join(format!("{slug}.{file_extension}"))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use tempfile::tempdir;

    #[test]
    fn config_keeps_store() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(config.store_path().to_str(), Some("/root"));
    }

    #[test]
    fn config_dot_arc_path() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(config.dot_arc_path().to_str(), Some("/root/.arc"));
    }

    #[test]
    fn config_documents_path() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(
            config.documents_path().to_str(),
            Some("/root/.arc/documents")
        );
    }

    #[test]
    fn config_document_path() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(
            config.document_path(1, "pdf").to_str(),
            Some("/root/.arc/documents/1.pdf")
        );
    }

    #[test]
    fn config_texts_path() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(config.texts_path().to_str(), Some("/root/.arc/texts"));
    }

    #[test]
    fn config_text_path() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(config.text_path(1).to_str(), Some("/root/.arc/texts/1.txt"));
    }

    #[test]
    fn config_extractors_path() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(
            config.extractors_path().to_str(),
            Some("/root/.arc/extractors")
        );
    }

    #[test]
    fn config_extractor_path() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(
            config.extractor_path("custom-extractor").to_str(),
            Some("/root/.arc/extractors/custom-extractor")
        );
    }

    #[test]
    fn config_extractor_path_accepts_absolute() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(
            config
                .extractor_path("/something/custom-extractor")
                .to_str(),
            Some("/something/custom-extractor")
        );
    }

    #[test]
    fn config_extracted_document_path() {
        let config = InitializationConfig::new(Path::new("/root"));
        assert_eq!(
            config
                .extracted_document_path(Slug::new("document"), "pdf")
                .to_str(),
            Some("/root/document.pdf")
        );
    }

    #[test]
    fn config_setup_creates_dot_arc() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        config.setup_directory_structure()?;
        assert!(config.dot_arc_path().exists());
        Ok(())
    }
}
