mod terminal_user_interaction;
use std::path::PathBuf;
use terminal_user_interaction::*;

use arc_api::{Arc, DocumentSelection};
use arc_core::{InitializationConfig, Slug};
use clap::{Parser, Subcommand};

#[derive(Parser, Debug)]
#[command(name = "Arc", author, about, version, propagate_version = true)]
/// Your Personal Archive Manager
///
/// Note that all commands require the environmental variable ARC_DOCUMENTS to be export and pointing to the store location.
struct Args {
    #[command(subcommand)]
    command: Option<Command>,
}

#[derive(clap::Args, Debug)]
#[group(multiple = false)]
struct DocumentSelectionArg {
    #[arg(short, long)]
    /// Select the latest document added.
    last: bool,
    /// Select the document specified by a slug, or query interactively if multiple documents with the same slug exist.
    #[arg(short, long)]
    slug: Option<Slug>,
}

impl From<DocumentSelectionArg> for DocumentSelection {
    fn from(d: DocumentSelectionArg) -> DocumentSelection {
        if d.last && d.slug.is_none() {
            DocumentSelection::Latest
        } else if !d.last && d.slug.is_some() {
            DocumentSelection::Slug(d.slug.unwrap())
        } else {
            DocumentSelection::Search
        }
    }
}

#[derive(Subcommand, Debug)]
enum Command {
    /// Initialize the store at the given location.
    ///
    /// Note that the location is given by the environmental variable ARC_DOCUMENTS.
    ///
    /// Example:
    ///
    ///   $ ARC_DOCUMENTS=. arc init # Initialize the store in the current working directory.
    #[command(verbatim_doc_comment)]
    Init,
    /// Show all documents
    ///
    /// Shows all the documents with the IDs, slugs and titles.
    ///
    /// Example:
    ///
    ///   $ arc show-all # Show all documents stored.
    #[command(verbatim_doc_comment)]
    ShowAll,
    /// Add a document to the store.
    ///
    /// Example:
    ///
    ///   $ arc add document.pdf # Add "document.pdf" to the store
    ///   $ arc add -i document.pdf # Add "document.pdf" to the store and modify the resulting document metainfo.
    #[command(verbatim_doc_comment, alias = "a")]
    Add {
        #[arg(required = true)]
        /// The path of the document.
        path: PathBuf,
        /// Edit the document after insertion.
        #[arg(short, long)]
        interactive: bool,
    },
    /// Edit a document.
    ///
    /// Example:
    ///
    ///   $ arc edit -l # Modify the last added document.
    ///   $ arc edit -s slug # Modify the document with the slug "slug".
    #[command(verbatim_doc_comment, alias = "e")]
    Edit {
        #[command(flatten)]
        selector: DocumentSelectionArg,
    },
    /// Open a document. This is the default if no subcommand was given.
    ///
    /// Example:
    ///
    ///   $ arc open -l # Modify the last added document.
    ///   $ arc open -s slug # Modify the document with the slug "slug".
    #[command(verbatim_doc_comment, alias = "o")]
    Open {
        #[command(flatten)]
        selector: DocumentSelectionArg,
    },
    /// Re-index a document.
    ///
    /// This will run the document extraction, tagging and finalization pipeline again for the specified document. If asked to do so, it may also replace the document in the store by a new document.
    /// This can be useful for documents that are updated regularly, you may not want to add every iteration but replace and redo the document itself.
    ///
    /// Example:
    ///
    ///   $ arc redo -l # Re-index the last document added.
    ///   $ arc redo -s slug document.pdf # Replace the stored document of slug "slug" with "document.pdf" and re-run indexing.
    #[command(verbatim_doc_comment, alias = "r")]
    Redo {
        #[command(flatten)]
        selector: DocumentSelectionArg,
        #[arg(short, long)]
        /// Edit the document after insertion.
        interactive: bool,
        #[arg(required = false)]
        /// The path of the document to replace the stored document with. If not given, the already stored document will be re-indexed.
        path: Option<PathBuf>,
    },
    /// Extract a document from the store, storing it in the root of the document store.
    ///
    /// The filename is computed by the slug and the file extension.
    ///
    /// Example:
    ///
    ///   $ arc extract -l # Extract the last added item into the root of the document store.
    #[command(verbatim_doc_comment)]
    Extract {
        #[command(flatten)]
        selector: DocumentSelectionArg,
    },
    /// Run a script on a document.
    ///
    /// Example:
    ///
    ///   $ arc script -l # Run on the last document
    ///   $ arc script # Run the script without a document unless it requires a document in which case it searches for one.
    #[command(verbatim_doc_comment, alias = "s")]
    Script {
        #[arg(required = true)]
        /// The name of the script.
        name: String,
        #[command(flatten)]
        selector: Option<DocumentSelectionArg>,
    },
}

// TODO: Error handling.
fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let args = Args::parse();
    log::info!("Got commandline arguments: {:#?}.", args);

    let Some(config) = InitializationConfig::from_env() else {
        log::error!("Failed to create initialization configuration from environment. Make sure you have the required environmental variables exported: {}", arc_core::ENV_DOCUMENTS);
        // TODO: Error
        return Ok(());
    };

    let command = args.command.unwrap_or(Command::Open {
        selector: DocumentSelectionArg {
            last: false,
            slug: None,
        },
    });

    if matches!(command, Command::Init) {
        config.setup_directory_structure()?;
        return Ok(());
    }

    let api: Arc<arc_store_sqlite::SqliteStore, TerminalUserInteraction> =
        Arc::new(config.clone(), TerminalUserInteraction::new(config))?;

    match command {
        Command::Init => {}
        Command::Add { path, interactive } => {
            api.add_document(path, interactive)?;
        }
        Command::Edit { selector } => {
            api.edit(&selector.into())?;
        }
        Command::Open { selector } => {
            api.open(&selector.into())?;
        }
        Command::Redo {
            selector,
            interactive,
            path,
        } => {
            api.redo(&selector.into(), interactive, path)?;
        }
        Command::Extract { selector } => {
            api.extract(&selector.into())?;
        }
        Command::ShowAll => {
            api.show_all()?;
        }
        Command::Script { name, selector } => {
            api.script(name, selector.map(|s| s.into()).as_ref())?;
        }
    }

    Ok(())
}
