use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
    io::{self},
    os::unix::process::CommandExt,
    process::Command,
};

use arc_core::{Document, InitializationConfig, SearchQuery, UserInteraction};

use crossterm::event::{DisableMouseCapture, EnableMouseCapture};
use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen,
};
use ratatui::{
    backend::CrosstermBackend,
    prelude::{Constraint, Layout},
    style::Style,
    widgets::{ListItem, ListState},
};
use ratatui::{
    prelude::Direction,
    widgets::{Block, Borders},
};
use ratatui::{widgets::List, Terminal};
use tui_textarea::{Input, Key, TextArea};

#[derive(Debug)]
pub enum TerminalUserInteractionError {
    RustyLine(rustyline::error::ReadlineError),
    Io(std::io::Error),
}

impl Display for TerminalUserInteractionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            TerminalUserInteractionError::RustyLine(e) => {
                write!(f, "failed to get user input from rustyline: {e}")
            }
            TerminalUserInteractionError::Io(e) => write!(f, "io error: {e}"),
        }
    }
}

impl From<rustyline::error::ReadlineError> for TerminalUserInteractionError {
    fn from(e: rustyline::error::ReadlineError) -> Self {
        Self::RustyLine(e)
    }
}

impl From<std::io::Error> for TerminalUserInteractionError {
    fn from(e: std::io::Error) -> Self {
        Self::Io(e)
    }
}

impl std::error::Error for TerminalUserInteractionError {}

pub struct TerminalUserInteraction {
    initialization: InitializationConfig,
}

impl TerminalUserInteraction {
    pub fn new(config: InitializationConfig) -> Self {
        Self {
            initialization: config,
        }
    }

    fn format_document_list(documents: &[Document]) -> String {
        let mut acc = String::new();
        // TODO: Not that efficient.
        for d in documents {
            acc += &format!("{}\t{}\t{}\n", d.id, d.slug, d.title);
        }
        acc
    }
}

impl UserInteraction for TerminalUserInteraction {
    type Error = TerminalUserInteractionError;

    fn open(&self, document: &arc_core::Document) -> Result<(), Self::Error> {
        let _ = Command::new("xdg-open")
            .arg(
                self.initialization
                    .document_path(document.id, &document.file_extension),
            )
            .process_group(0)
            .spawn()?;
        Ok(())
    }

    fn show(&self, document: &arc_core::Document) -> Result<(), Self::Error> {
        println!("Id: {}", document.id);
        println!("Slug: {}", document.slug);
        println!("Title: {}", document.title);
        println!(
            "Tags: {}",
            document
                .tags
                .iter()
                .map(|s| s.to_string())
                .collect::<Vec<_>>()
                .join(" ")
        );
        println!(
            "Data: {}",
            document
                .data
                .iter()
                .map(|(k, v)| format!("{}:{}", k, v))
                .collect::<Vec<_>>()
                .join(" ")
        );
        Ok(())
    }

    fn edit(&self, doc: arc_core::Document) -> Result<arc_core::Document, Self::Error> {
        let mut doc = doc;
        log::debug!("Asking user to modify document.");
        let mut rl = rustyline::DefaultEditor::new()?;
        log::trace!("Reading slug. Old slug: {}", doc.title);
        doc.slug = rl
            .readline_with_initial("New Slug: ", (&String::from(doc.slug), ""))?
            .into();
        log::trace!("Edit slug: {}.", doc.slug);
        log::trace!("Reading title. Old title: {}", doc.title);
        doc.title = rl.readline_with_initial("New Title: ", (&doc.title, ""))?;
        log::trace!("Edit Title: {}.", doc.title);

        log::trace!("Reading tags.");
        let old_tags = doc.tags.clone();
        let mut new_tags = HashSet::new();
        // TODO: Sort
        for t in old_tags {
            log::trace!("Prompting for tag: {t}.");
            let newt =
                rl.readline_with_initial("Edit Tag (empty to delete): ", (&t.to_string(), ""))?;
            if !newt.is_empty() {
                log::trace!("Edited tag to: {newt}.");
                new_tags.insert(newt.into());
            } else {
                log::trace!("Deleting tag.");
            }
        }

        log::trace!("Asking for new tags.");
        let mut newt = rl.readline_with_initial("New Tag (empty to finish): ", ("", ""))?;
        while !newt.is_empty() {
            log::trace!("Got new tag {newt}.");
            new_tags.insert(newt.into());
            newt = rl.readline_with_initial("New Tag (empty to finish): ", ("", ""))?;
        }
        log::trace!("Finished asking for tags.");

        doc.tags = new_tags;

        log::trace!("Reading data.");
        let old_data = doc.data.clone();
        let mut new_data = HashMap::new();
        // TODO: Sort
        for (k, v) in old_data.iter() {
            let mut success = false;
            while !success {
                log::trace!("Prompting for data: {k}:{v}.");
                let newd = rl.readline_with_initial(
                    "Edit data (empty to delete): ",
                    (&format!("{k}:{v}"), ""),
                )?;
                if !newd.is_empty() {
                    log::trace!("Edited data to: {newt}.");
                    if let Some((key, value)) = newd.split_once(':') {
                        new_data.insert(key.into(), value.to_string());
                        success = true;
                    } else {
                        println!("Failed to input key-value data. Make sure the input is separated by a colon.");
                    }
                } else {
                    log::trace!("Deleting data.");
                    success = true;
                }
            }
        }

        log::trace!("Asking for new data.");
        let mut newd = rl.readline_with_initial("New Data (empty to finish): ", ("", ""))?;
        while !newd.is_empty() {
            if let Some((key, value)) = newd.split_once(':') {
                new_data.insert(key.into(), value.to_string());
            } else if !newd.is_empty() {
                println!(
                    "Failed to input key-value data. Make sure the input is separated by a colon."
                );
            }
            newd = rl.readline_with_initial("New Data (empty to finish): ", ("", ""))?;
        }
        log::trace!("Finished asking for data.");

        doc.data = new_data;

        Ok(doc)
    }

    fn show_all(&self, documents: &[arc_core::Document]) -> Result<(), Self::Error> {
        println!("{}", Self::format_document_list(documents));
        Ok(())
    }

    fn search<F: Fn(arc_core::SearchQuery) -> Vec<Document>>(
        &self,
        searcher: F,
    ) -> Result<Option<Document>, Self::Error> {
        let stdout = io::stdout();
        let mut stdout = stdout.lock();

        enable_raw_mode()?;
        crossterm::execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
        let backend = CrosstermBackend::new(stdout);
        let mut term = Terminal::new(backend)?;

        let layout = Layout::default()
            .direction(Direction::Vertical)
            .margin(1)
            .constraints([Constraint::Length(3), Constraint::Min(1)].as_slice());

        let mut textarea = TextArea::default();
        textarea.set_cursor_line_style(Style::default());
        textarea.set_placeholder_text("Search");
        textarea.set_block(Block::default().borders(Borders::ALL));

        let mut displayed_documents = searcher(SearchQuery::default());
        let mut selected = 0;

        let mut result = None;

        loop {
            term.draw(|f| {
                let chunks = layout.split(f.size());
                let widget = textarea.widget();
                f.render_widget(widget, chunks[0]);

                let list = List::new(
                    displayed_documents
                        .iter()
                        .map(|d| {
                            ListItem::new(if d.title.is_empty() {
                                d.slug.to_string()
                            } else {
                                d.title.to_string()
                            })
                        })
                        .collect::<Vec<_>>(),
                )
                .highlight_symbol(">> ")
                .block(Block::default().borders(Borders::ALL));

                f.render_stateful_widget(
                    list,
                    chunks[1],
                    &mut ListState::default().with_selected(Some(selected)),
                );
            })?;
            match crossterm::event::read()?.into() {
                Input { key: Key::Esc, .. } => break,
                Input {
                    key: Key::Enter, ..
                } => {
                    result = displayed_documents.get(selected).cloned();
                    break;
                }
                Input {
                    key: Key::Char('n'),
                    ctrl: true,
                    ..
                }
                | Input { key: Key::Down, .. } => {
                    selected = (selected + 1) % displayed_documents.len();
                }
                Input {
                    key: Key::Char('p'),
                    ctrl: true,
                    ..
                }
                | Input { key: Key::Up, .. } => {
                    selected =
                        (displayed_documents.len() + selected - 1) % displayed_documents.len();
                }
                input => {
                    textarea.input(input);
                    let search = &textarea.lines()[0];
                    let query = SearchQuery::parse(search);
                    displayed_documents = searcher(query);
                }
            }
        }

        disable_raw_mode()?;
        crossterm::execute!(
            term.backend_mut(),
            LeaveAlternateScreen,
            DisableMouseCapture
        )?;
        term.show_cursor()?;

        Ok(result)
    }
}
