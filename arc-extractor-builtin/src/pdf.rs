use std::{
    fmt::Display,
    path::Path,
    process::{Command, Stdio},
};

use arc_core::{ExtractionError, ExtractionOutput, Extractor, InitializationConfig};

use serde::Deserialize;

const PDFTOTEXT: &str = "pdftotext";

pub struct PdfExtractor {
    config: InitializationConfig,
}

impl PdfExtractor {
    pub fn new(config: InitializationConfig) -> Self {
        log::debug!("Setting up an pdf extractor.",);
        Self { config }
    }

    pub fn extract_title<P: AsRef<Path>>(
        &self,
        current_document_path: P,
    ) -> Result<String, ExtractionError> {
        let current_document_path = current_document_path.as_ref();
        log::trace!("Building the command to extract the title");
        let mut command = Command::new(PDFTOTEXT);
        // The tsv output also gives the font sizes for each word.
        log::trace!("Setting the command to tsv mode.");
        command.arg("-f").arg("1");
        command.arg("-l").arg("1");
        command.arg("-tsv");

        log::trace!(
            "Setting input document path: {}",
            current_document_path.display()
        );
        command.arg(current_document_path);

        log::trace!("Setting output to stdout.",);
        command.arg("-");

        // Pipes
        log::trace!("Setting stdin to null and stdout pipe to pipe.");
        command.stdin(Stdio::null());
        command.stdout(Stdio::piped());

        log::debug!("Running extractor command: {:#?}", command);
        let result = command
            .spawn()
            .map_err(|e| ExtractionError::ExtractorError(Box::new(PdfExtractorError::from(e))))?
            .wait_with_output()
            .map_err(|e| ExtractionError::ExtractorError(Box::new(PdfExtractorError::from(e))))?;

        if !result.status.success() {
            log::error!(
                "The external command did not exit cleanly: Exit code {}.",
                result.status.code().unwrap_or(-1)
            );
            return Err(PdfExtractorError::PdfToTextExitedFailure(
                result.status.code().unwrap_or(-1),
            )
            .into());
        }

        log::trace!(
            "Extractor created output: {}",
            String::from_utf8_lossy(&result.stdout[..])
        );

        let records = csv::ReaderBuilder::new()
            .delimiter(b'\t')
            .has_headers(true)
            .from_reader(&result.stdout[..])
            .deserialize()
            .collect::<Result<Vec<PdfToTextTsvRecord>, _>>()
            .map_err(PdfExtractorError::Csv)?;

        log::trace!("Found records: {:#?}", records);

        Ok(extract_title_from_records(&records))
    }
}

#[derive(Debug)]
pub enum PdfExtractorError {
    Io(std::io::Error),
    PdfToTextExitedFailure(i32),
    Csv(csv::Error),
}

impl Display for PdfExtractorError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(e) => write!(f, "spawing an external extractor failed: {e}")?,
            Self::PdfToTextExitedFailure(e) => {
                write!(f, "the command '{}' exited with failure: {}", PDFTOTEXT, e)?
            }
            Self::Csv(e) => write!(f, "parsing the tsv-output of {} failed: {}.", PDFTOTEXT, e)?,
        }
        Ok(())
    }
}

impl std::error::Error for PdfExtractorError {}

impl From<std::io::Error> for PdfExtractorError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<PdfExtractorError> for ExtractionError {
    fn from(value: PdfExtractorError) -> Self {
        ExtractionError::ExtractorError(Box::new(value))
    }
}

impl Extractor for PdfExtractor {
    fn extract(
        &self,
        document: arc_core::ExtractionInput,
    ) -> Result<arc_core::ExtractionOutput, arc_core::ExtractionError> {
        log::debug!("Calling PDF extractor on input: {:#?}.", document);

        if document.mime_type != mime::APPLICATION_PDF {
            log::error!(
                "Called PDF extractor on a mime type that is not PDF: {}",
                document.mime_type
            );
            return Err(ExtractionError::InvalidMimeType(
                document.mime_type,
                mime::APPLICATION_PDF,
            ));
        }

        log::trace!("Building the command to extract the text");
        let mut command = Command::new(PDFTOTEXT);

        let current_document_path = self
            .config
            .document_path(document.id, &document.file_extension);
        let current_text_path = self.config.text_path(document.id);

        log::trace!(
            "Setting input document path: {}",
            current_document_path.display()
        );
        command.arg(&current_document_path);
        log::trace!("Setting output text path: {}", current_text_path.display());
        command.arg(current_text_path);

        // Pipes
        log::trace!("Setting stdin and stdout pipes to null.");
        command.stdin(Stdio::null());
        command.stdout(Stdio::null());

        log::debug!("Running external command: {:#?}", command);
        let status = command
            .spawn()
            .map_err(|e| ExtractionError::ExtractorError(Box::new(PdfExtractorError::from(e))))?
            .wait()
            .map_err(|e| ExtractionError::ExtractorError(Box::new(PdfExtractorError::from(e))))?;

        if !status.success() {
            log::error!(
                "The external command did not exit cleanly: Exit code {}.",
                status.code().unwrap_or(-1)
            );
            return Err(
                PdfExtractorError::PdfToTextExitedFailure(status.code().unwrap_or(-1)).into(),
            );
        }

        Ok(ExtractionOutput {
            title: self.extract_title(&current_document_path)?,
        })
    }
}

#[derive(Deserialize, Debug)]
struct PdfToTextTsvRecord {
    height: f32,
    conf: i32,
    text: String,
}

fn extract_title_from_records(records: &[PdfToTextTsvRecord]) -> String {
    log::trace!("Extracting the title from the records");
    let mut title = String::new();
    let mut max_height = 0.0;
    let mut concurrent = false;

    for r in records {
        if r.conf > 0 && concurrent && r.height == max_height {
            title += &format!(" {}", r.text);
        } else if r.conf > 0 && r.height > max_height {
            title = r.text.clone();
            max_height = r.height;
            concurrent = true;
        } else {
            concurrent = false;
        }
    }

    title
}
