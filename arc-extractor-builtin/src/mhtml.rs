use std::{
    fmt::Display,
    fs::File,
    io::{Read, Write},
    str::FromStr,
};

use arc_core::{ExtractionError, ExtractionOutput, Extractor, InitializationConfig};

use mail_parser::{MessageParser, PartType};
use mime::Mime;
use regex::Regex;
use scraper::Html;

pub struct MhtmlExtractor {
    config: InitializationConfig,
}

impl MhtmlExtractor {
    pub fn new(config: InitializationConfig) -> Self {
        log::debug!("Setting up an pdf extractor.",);
        Self { config }
    }
}

#[derive(Debug)]
pub enum MhtmlParserError {
    FailedDocumentParsing,
    NoHtmlPart,
}

#[derive(Debug)]
pub enum MhtmlExtractorError {
    Io(std::io::Error),
    MhtmlParser(MhtmlParserError),
}

impl Display for MhtmlExtractorError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(e) => write!(f, "spawing an external extractor failed: {e}")?,
            Self::MhtmlParser(MhtmlParserError::FailedDocumentParsing) => {
                write!(f, "failed to parse document as mhtml")?
            }
            Self::MhtmlParser(MhtmlParserError::NoHtmlPart) => {
                write!(f, "the mhtml file does not have a html part")?
            }
        }
        Ok(())
    }
}

impl std::error::Error for MhtmlExtractorError {}

impl From<std::io::Error> for MhtmlExtractorError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<MhtmlExtractorError> for ExtractionError {
    fn from(value: MhtmlExtractorError) -> Self {
        ExtractionError::ExtractorError(Box::new(value))
    }
}

impl Extractor for MhtmlExtractor {
    fn extract(
        &self,
        document: arc_core::ExtractionInput,
    ) -> Result<arc_core::ExtractionOutput, arc_core::ExtractionError> {
        log::debug!("Calling MHTML extractor on input: {:#?}.", document);
        if document.mime_type.type_() != "message" && document.mime_type.subtype() != "rfc822" {
            log::error!(
                "Called MHTML extractor on a mime type that is not MHTML: {}",
                document.mime_type
            );
            return Err(ExtractionError::InvalidMimeType(
                document.mime_type,
                Mime::from_str("message/rfc822").expect("parseable mime type"),
            ));
        }

        let current_document_path = self
            .config
            .document_path(document.id, document.file_extension);

        let mut file = File::open(current_document_path).map_err(MhtmlExtractorError::Io)?;
        let mut bytes = Vec::new();

        file.read_to_end(&mut bytes)
            .map_err(MhtmlExtractorError::Io)?;

        let message =
            MessageParser::default()
                .parse(&bytes)
                .ok_or(MhtmlExtractorError::MhtmlParser(
                    MhtmlParserError::FailedDocumentParsing,
                ))?;

        let title = message.subject().unwrap_or_default().to_string();

        // XXX: Maybe also inspect other html parts?
        let html_part = message
            .html_part(0)
            .ok_or(MhtmlExtractorError::MhtmlParser(
                MhtmlParserError::NoHtmlPart,
            ))?;
        let PartType::Html(ref html_part_body) = html_part.body else {
            return Err(MhtmlExtractorError::MhtmlParser(MhtmlParserError::NoHtmlPart).into());
        };

        let html = Html::parse_document(html_part_body);
        let text = html
            .root_element()
            .text()
            // .intersperse("\n")
            .collect::<String>();

        let regex_replace_multispace = Regex::new(r"\s\s+").expect("parseable regex");
        let text = regex_replace_multispace.replace_all(&text, "\n");

        let text_file_path = self.config.text_path(document.id);
        let mut text_file = File::create(text_file_path).map_err(MhtmlExtractorError::Io)?;

        text_file
            .write(&text.to_string().into_bytes())
            .map_err(MhtmlExtractorError::Io)?;

        Ok(ExtractionOutput { title })
    }
}
