use std::path::Path;

use arc_core::InitializationConfig;
use arc_extractor_builtin::PdfExtractor;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    env_logger::init();
    let config = InitializationConfig::from_env().expect("Documents directory not set");
    let extractor = PdfExtractor::new(config);

    let file = std::env::args().nth(1).expect("Argument of PDF path");
    let path = Path::new(&file);

    let title = extractor.extract_title(path)?;

    println!("Result title: {title}");
    Ok(())
}
