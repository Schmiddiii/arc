use std::{fs::File, path::Path};

use convert_case::{Case, Casing};

pub fn sluggify<S: AsRef<str>>(s: S) -> String
where
    String: PartialEq<S>,
{
    log::trace!("Sluggifying: {}", s.as_ref());
    Casing::to_case(&s, Case::Kebab)
}

pub fn create_dir_if_not_exists<P: AsRef<Path>>(path: P) -> Result<(), std::io::Error> {
    log::trace!(
        "Creating directory if it does not yet exist: {}.",
        path.as_ref().display()
    );
    if !path.as_ref().exists() {
        std::fs::create_dir(path)?;
    }
    Ok(())
}

pub fn create_all_dir_if_not_exists<P: AsRef<Path>>(paths: &[P]) -> Result<(), std::io::Error> {
    log::trace!("Creating multile paths if they do not yet exist.",);
    for p in paths {
        create_dir_if_not_exists(p)?;
    }
    Ok(())
}

pub fn delete_if_exists<P: AsRef<Path>>(path: P) -> Result<(), std::io::Error> {
    log::trace!("Deleting path if it exists: {}.", path.as_ref().display());
    if path.as_ref().exists() {
        log::trace!("Path does exist. Deleting.");
        std::fs::remove_file(path)?;
    }
    Ok(())
}

pub fn change_read_only<P: AsRef<Path>>(path: P, read_only: bool) -> Result<(), std::io::Error> {
    log::trace!(
        "Changing file permissions to read-only: {}.",
        path.as_ref().display()
    );
    if !path.as_ref().exists() {
        log::trace!("Path does not exist. Not changing permission.");
        return Ok(());
    }
    let file = File::open(&path)?;
    let mut permissions = file.metadata()?.permissions();
    permissions.set_readonly(read_only);
    file.set_permissions(permissions)?;
    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn sluggify_basic() {
        assert_eq!("my-file", sluggify("My file"));
        assert_eq!("my-file", sluggify("MyFile"));
    }

    #[test]
    fn sluggify_with_dash() {
        assert_eq!("my-file", sluggify("My - File"))
    }
}
