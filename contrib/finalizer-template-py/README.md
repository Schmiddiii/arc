# Finalizer Template (Python)

The `finalizer_template.py` script provides a template for custom finalizers written in Python.

## Usage

- Edit the methods `condition` and `modify` of the script to your liking.
- Move the script to `.arc/finalizers/` with some descriptive name.
- Add your finalizer to some mime type as documented in the project.
