#!/usr/bin/env python3

import sys
import json
import os

class Input:
    """The main class containing the document slug, title, tags and data and also finalizer configuration."""
    def __init__(self, slug, title, tags, data, config):
        self.slug = slug
        self.title = title
        self.tags = tags
        self.data = data
        self.config = config

    def document_path(self):
        """Provides the path to the document file of the current document."""
        return os.environ["ARC_CURRENT_DOCUMENT"]

    def text_path(self):
        """Provides the path to the text file of the current document."""
        return os.environ["ARC_CURRENT_TEXT"]

    def from_json(doc_json):
        return Input(doc_json['slug'], doc_json['title'], doc_json['tags'], doc_json['data'], doc_json['config'])

    def to_json(self):
        return json.dumps({
            "slug": self.slug,
            "title": self.title,
            "tags": self.tags,
            "data": self.data
        })

def condition(doc):
    """Check if the document specified in the input should be changed.

    Example:

    ```py
    return "some-tag" in doc.tags
    ```
    
    :param doc: The input to the script.
    :return: `True` if the document should be changed, `False` otherwise.
    """
    pass

def modify(doc):
    """Modify a document specified in the input.

    Example:

    ```py
    doc.title = "Changed Title"
    return doc
    ```
    
    :param doc: The input containing the document to change.
    :return: The modified input containing the document.
    """
    pass

# Process the incoming document.
for s in sys.stdin:
    # Load it from stding.
    doc = Input.from_json(json.loads(s))
    # If it requires a change, modify it.
    if condition(doc):
        doc = modify(doc)
    # Return the document as json.
    print(doc.to_json())

