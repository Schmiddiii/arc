# Arc Contrib

This directory contains user-submitted content related to Arc. Anyone is free to open a request to add contributions by opening a MR (note the requirements for submission below).

Note that these contributions may not work in all setup, e.g. due to updates to Arc.

## Contents

| Name                    | Type                 | Description                                         |
|-------------------------|----------------------|-----------------------------------------------------|
| finalizer-template-py   | finalizer (template) | A template for writing custom finalizers in python. |
| script-template-py      | script (template)    | A template for writing custom scripts in python.    |
| qutebrowser-integration | integration          | Integrate Arc into qutebrowser.                     |
| 2rm2                    | script               | Copy files to your reMarkable2.                     |

## Requirements for Submission

- Your contribution must be in a self-contained folder.
- This folder must contain a `README.md` giving at least the name (as the title), a one-line description, usage instructions and configuration options (may be omitted if it does not support configuration).
- Addition of your contribution in the above table.
- Note that the `LICENSE.md` from the root of the repository applies.
- Note that [Schmiddiii](https://gitlab.com/Schmiddiii) (and potentially other repository contributers) may remove your contribution for any reason. This may e.g. include it being outdated with the repository version. I probably won't remove it without at least informing you beforehand, but in case you don't respond, I will remove it.
