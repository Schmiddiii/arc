#!/usr/bin/env python3

import sys
import json
import os
import requests

class Input:
    """The main class containing the document slug, title, tags and data and also finalizer configuration."""
    def __init__(self, id, slug, file_extension, mime_type, title, tags, data, config):
        self.id = id
        self.slug = slug
        self.file_extension = file_extension
        self.mime_type = mime_type
        self.title = title
        self.tags = tags
        self.data = data
        self.config = config

    def document_path(self):
        """Provides the path to the document file of the current document."""
        return os.environ["ARC_CURRENT_DOCUMENT"]

    def text_path(self):
        """Provides the path to the text file of the current document."""
        return os.environ["ARC_CURRENT_TEXT"]

    def from_json(doc_json):
        return Input(doc_json['id'], doc_json['slug'], doc_json['file_extension'], doc_json['mime_type'], doc_json['title'], doc_json['tags'], doc_json['data'], doc_json['config'])

def do_with_document(doc):
    """
    Execute if an entire document was given.
    """
    with open(doc.document_path(), mode="rb") as file:
        payload = {
            # Tuple of the file name,the file itself and the mime type.
            "file": (doc.title, file, doc.mime_type),
        }

        path = "/"
        if "path" in doc.config:
            path = path + doc.config["path"]
        if not path.endswith("/"):
            path = path + "/"

        requests.get("http://10.11.99.1/documents" + path)
        requests.post("http://10.11.99.1/upload", files=payload)

def do_with_config(config):
    """
    Execute if no document was given.
    """
    print("A document has to be given")
    exit(1)

# Process the incoming document.
for s in sys.stdin:
    data = json.loads(s)
    if "id" in data:
        doc = Input.from_json(data)
        do_with_document(doc)
    else:
        do_with_config(data['config'])
