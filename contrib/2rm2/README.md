# 2rm2

Upload files from the store to your reMarkable2 using the USB web interface.

Note that only PDF files have been tested, but epub should also work.

## Usage

- Install the script as documented in the project. Note that the script requires a document to be given (note the configuration below).
- Plug in your reMarkable2 using an USB cable and enable the USB web interface (note that this disables itself after every reboot).
- Run the script from arc and select your document. It should be on your reMarkable (in the root or in the path you specified).

## Configuration

This script has one possible configuration parameter, named `path`. This specifies the location where the document should be uploaded. Note that this path is the internal path of the reMarkable2 and not the user-facing way. To figure out your desired path, do the following:

- Connect your reMarkable2 using the USB web interface.
- Execute `curl http://10.11.99.1/documents/` from the command line. Search the output for your desired folder and note the `ID` of it.
- If you want to upload to a subfolder, execute `curl http://10.11.99.1/documents/$id/` (replacing the `ID` you noted). Recurse as far as you want (separating the IDs with `/`).

You should therefore now have a list of UUIDs separated by `/` as your path. Set this as the `path` in the configuration. An example configuration could be:

```toml
[scripts."2rm2"]
type = "2rm2.py"
requires_document = true
[scripts."2rm2".config]
path = "81b66c5-12a1-15f6-f55a-68bb81186f8/8ab66c5-a2a1-a5f6-f55a-68bb81186f8"
```
