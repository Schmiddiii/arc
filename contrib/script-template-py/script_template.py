#!/usr/bin/env python3

import sys
import json
import os

class Input:
    """The main class containing the document slug, title, tags and data and also finalizer configuration."""
    def __init__(self, id, slug, file_extension, mime_type, title, tags, data, config):
        self.id = id
        self.slug = slug
        self.file_extension = file_extension
        self.mime_type = mime_type
        self.title = title
        self.tags = tags
        self.data = data
        self.config = config

    def document_path(self):
        """Provides the path to the document file of the current document."""
        return os.environ["ARC_CURRENT_DOCUMENT"]

    def text_path(self):
        """Provides the path to the text file of the current document."""
        return os.environ["ARC_CURRENT_TEXT"]

    def from_json(doc_json):
        return Input(doc_json['id'], doc_json['slug'], doc_json['file_extension'], doc_json['mime_type'], doc_json['title'], doc_json['tags'], doc_json['data'], doc_json['config'])

def do_with_document(doc):
    """
    Execute if an entire document was given.
    """
    pass

def do_with_config(config):
    """
    Execute if no document was given.
    """
    pass

# Process the incoming document.
for s in sys.stdin:
    data = json.loads(s)
    if "id" in data:
        doc = Input.from_json(data)
        do_with_document(doc)
    else:
        do_with_config(data['config'])
