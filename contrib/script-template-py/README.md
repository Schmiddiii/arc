# Script Template (Python)

The `script_template_input.py` script provides a template for custom script that (optionally) takes documents written in Python.

## Usage

- Edit the functions `do_with_document` and `do_with_config` as desired.
- Move the script to `.arc/scripts/` with some descriptive name.
- Add your script as documented in the project.
