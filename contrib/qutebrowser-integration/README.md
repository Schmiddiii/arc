# Qutebrowser Integration

Integration of Arc for [qutebrowser](https://qutebrowser.org/). Download files directly into your Arc store.

## Usage

- Copy `arc.py` into `~/.config/qutebrowser/userscripts/`.
- On a website you want to download, run `:spawn --userscript arc.py`.

Note: This assumes your `ARC_DOCUMENTS` is set up before qutebrowser is spawned. Alternatively, add that environmental variable to the `subprocess.run` as [documented in the subprocess documentation](https://docs.python.org/3/library/subprocess.html).

Note: You may also want to add a keybinding to download a website.

Note: Make sure you have an extractor (e.g. `builtin/mhtml`) set up for `message/rfc822`.
