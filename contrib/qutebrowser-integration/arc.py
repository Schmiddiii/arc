#!/usr/bin/env python3

import sys
import os
import secrets
import time
import subprocess

arc_bin="arc"
wait_between_file_exists_checks_s=1

fifo_path = os.environ["QUTE_FIFO"]

tmp_download_path = "/tmp/qute-arc-download-" + secrets.token_hex(16) + ".mhtml"

with open(fifo_path, 'w') as fifo:
    print("download --mhtml --dest " + tmp_download_path, file=fifo, flush=True)

    while not os.path.isfile(tmp_download_path):
        time.sleep(wait_between_file_exists_checks_s)
    
    subprocess.run([arc_bin, "add", tmp_download_path])

    print("download-clear", file=fifo, flush=True)
    os.remove(tmp_download_path)
