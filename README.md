# Arc

Your personal archive manager.

__Note__: Currently only in the alpha phase. Note the section about support below.

## What is it?

Arc is your personal little buddy to help you archive, sort and find documents. It tries to be extensible, highly configurable and completely local. A typical workflow consists of:

- Adding a document using the CLI.
- The document will automatically be assigned tags, data, title and more.
- The document text will automatically be extracted from the document.
- You can edit the metainfo if desired, but that should rarely be needed (assuming you have a good configuration).
- You can search through all your documents via their metainfo and also full-text search accross their contents.

All that while being:

- Completely local.
- Extensible to any file-type.
- Scriptable for custom rules for tagging, naming and searching (searching is still to do).

## Example

Add some documents, which will automatically extract information:

```sh
$ arc add top-secret-document.pdf
Id: 1
Slug: secret-about-secrets
Title: Top Secret Information about Secrets
Tags: top-secret pdf 
Data: added:2023-10-15T14:04:08 classification:5

$ arc add bottom-secret-document.pdf
Id: 2
Slug: not-secret-information
Title: Bottom Secret Information
Tags: bottom-secret pdf 
Data: added:2023-10-15T14:05:08 classification:0
```

Search for some documents:

```sh
$ arc open
# A TUI will open presenting a search entry and a list of matching documents.
# You can navigate the list of all matching documents and open the one you want.
# The below lines are all possible search terms for finding the top-secret document.
Top Secret                                                          # Search for the title
Secret #top-secret                                                  # Search for the title and also a tag.
Secrets #classification:5                                           # Search for the title and a datapoint.
#classification:5 @before:week                                      # Search for a datapoint and apply a custom filter (to search only for documents before a given date).
Secret - Bames Jond #classification:5 #top-secret @before:week      # Search for the title, the content, the tag, the datapoint and apply a custom filter.
```

## Configuration

By default, Arc is completely unconfigured and will not be able to do any of the wonderful things I mentioned. Consult the documentation in `doc` for more information.

Also note that using Arc will require the environmental variable `ARC_DOCUMENTS` to be pointed the folder where to store your documents at all time.

## Support

This is a free-time project, created mainly for personal use. Even though I try to be helpful, I will only implement features if I consider them useful for myself. The same holds for merge requests (pull requests to `contrib` are always welcome though, as long as they follow the rules). After all, document management is a process that is different for every person, and I don't want this tool to be filled with features not useful for me.