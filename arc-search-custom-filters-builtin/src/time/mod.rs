use std::collections::HashMap;

use arc_core::{Document, SearchCustomFilter};
use chrono::{Days, Local, Months, NaiveDate};

struct TimeFilterConfig {
    document_key: String,
    document_format: String,
    default_value: String,
    // Human parse format?
    // Time offset config?
}

impl From<&HashMap<String, String>> for TimeFilterConfig {
    fn from(value: &HashMap<String, String>) -> Self {
        let def = Self::default();
        Self {
            document_key: value
                .get("document_key")
                .unwrap_or(&def.document_key)
                .to_owned(),
            document_format: value
                .get("document_format")
                .unwrap_or(&def.document_format)
                .to_owned(),
            default_value: value
                .get("default_value")
                .unwrap_or(&def.default_value)
                .to_owned(),
        }
    }
}

impl Default for TimeFilterConfig {
    fn default() -> Self {
        Self {
            document_key: "added".to_owned(),
            document_format: "%+".to_owned(),
            default_value: "yesterday".to_owned(),
        }
    }
}

fn parse_document_time(config: &TimeFilterConfig, document: &Document) -> Option<NaiveDate> {
    let value = document.data.get(&config.document_key.clone().into());
    value.and_then(|s| NaiveDate::parse_from_str(s, &config.document_format).ok())
}

fn parse_human_time<S: AsRef<str>>(arg: S) -> NaiveDate {
    match arg.as_ref() {
        "today" => Local::now().date_naive(),
        "yesterday" => (Local::now() - Days::new(1)).date_naive(),
        "week" => (Local::now() - Days::new(7)).date_naive(),
        "month" => (Local::now() - Months::new(1)).date_naive(),
        "year" => (Local::now() - Months::new(12)).date_naive(),
        // TODO: On fail, use yesterday?
        s => s
            .parse::<NaiveDate>()
            .ok()
            .unwrap_or((Local::now() - Days::new(1)).date_naive()),
    }
}

pub struct BeforeTimeFilter(TimeFilterConfig);

impl BeforeTimeFilter {
    pub fn new(config: &HashMap<String, String>) -> Self {
        Self(config.into())
    }
}

impl SearchCustomFilter for BeforeTimeFilter {
    fn matches(&self, document: &Document, arg: Option<&str>) -> bool {
        let Some(date_document) = parse_document_time(&self.0, document) else {
            log::warn!("Failed to parse document time");
            return false;
        };
        let date_filter = parse_human_time(arg.unwrap_or(&self.0.default_value[..]));

        date_document <= date_filter
    }
}

pub struct AfterTimeFilter(TimeFilterConfig);

impl AfterTimeFilter {
    pub fn new(config: &HashMap<String, String>) -> Self {
        Self(config.into())
    }
}

impl SearchCustomFilter for AfterTimeFilter {
    fn matches(&self, document: &arc_core::Document, arg: Option<&str>) -> bool {
        let Some(date_document) = parse_document_time(&self.0, document) else {
            return false;
        };
        let date_filter = parse_human_time(arg.unwrap_or(&self.0.default_value[..]));

        date_document >= date_filter
    }
}

#[cfg(test)]
mod test {
    use std::collections::{HashMap, HashSet};

    use chrono::{DateTime, Duration};

    use super::*;

    fn document_at(time: DateTime<Local>) -> Document {
        Document {
            id: 1,
            slug: "test".into(),
            file_extension: "md".into(),
            mime_type: mime::APPLICATION_PDF,
            title: "Test".into(),
            tags: HashSet::new(),
            data: HashMap::from([("added".into(), time.format("%+").to_string())]),
        }
    }

    #[test]
    fn time_filter_human_today() {
        let before = BeforeTimeFilter(TimeFilterConfig::default());
        assert!(before.matches(&document_at(Local::now() - Days::new(2)), Some("today")));
        assert!(!before.matches(&document_at(Local::now() + Days::new(1)), Some("today")));
    }

    #[test]
    fn time_filter_human_yesterday() {
        let before = BeforeTimeFilter(TimeFilterConfig::default());
        assert!(before.matches(&document_at(Local::now() - Days::new(2)), Some("yesterday")));
        assert!(!before.matches(&document_at(Local::now()), Some("yesterday")));
    }

    #[test]
    fn time_filter_human_week() {
        let before = BeforeTimeFilter(TimeFilterConfig::default());
        assert!(before.matches(&document_at(Local::now() - Days::new(8)), Some("week")));
        assert!(!before.matches(&document_at(Local::now() + Duration::days(7)), Some("week")));
    }

    #[test]
    fn time_filter_human_month() {
        let before = BeforeTimeFilter(TimeFilterConfig::default());
        assert!(before.matches(&document_at(Local::now() - Months::new(2)), Some("week")));
        assert!(!before.matches(&document_at(Local::now()), Some("week")));
    }

    #[test]
    fn time_filter_human_year() {
        let before = BeforeTimeFilter(TimeFilterConfig::default());
        assert!(before.matches(&document_at(Local::now() - Months::new(13)), Some("year")));
        assert!(!before.matches(&document_at(Local::now() - Months::new(11)), Some("year")));
    }

    #[test]
    fn time_filter_human_custom_1() {
        let before = BeforeTimeFilter(TimeFilterConfig::default());
        let arg = (Local::now() - Months::new(1))
            .format("%Y-%m-%d")
            .to_string();
        assert!(before.matches(&document_at(Local::now() - Months::new(2)), Some(&arg)));
        assert!(!before.matches(&document_at(Local::now()), Some(&arg)));
    }
    #[test]
    fn time_filter_human_custom_2() {
        let before = BeforeTimeFilter(TimeFilterConfig::default());
        let arg = (Local::now() - Months::new(1))
            .format("%d.%m.%Y")
            .to_string();
        assert!(before.matches(&document_at(Local::now() - Months::new(2)), Some(&arg)));
        assert!(!before.matches(&document_at(Local::now()), Some(&arg)));
    }
}
