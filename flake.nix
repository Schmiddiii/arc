{
  description = "Your personal archive manager";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils, ... }@inputs:
    (flake-utils.lib.eachDefaultSystem
      (system:
        let
          pkgs = import nixpkgs {
            inherit system;
          };
          name = "arc";
        in
        rec { 
          packages.default = 
            with pkgs;
            stdenv.mkDerivation rec {
              cargoDeps = rustPlatform.importCargoLock {
                lockFile = ./Cargo.lock;
              };

              buildPhase = ''
                cargo build --release --offline
              '';

              installPhase = ''
                mkdir -p $out/bin
                mv target/release/${name} $out/bin/
              '';

              src = ./.;
              buildInputs = with pkgs; [ sqlite ];
              nativeBuildInputs = with pkgs; [ rustPlatform.cargoSetupHook cargo rustc xdg-utils ];

              inherit name;
            };
          devShells.default =
            let 
              run = pkgs.writeShellScriptBin "run" ''
                export ARC_DOCUMENTS=./documents
                cargo run -- "$@"
              '';
              check = pkgs.writeShellScriptBin "check" ''
                cargo clippy
              '';
            in
            with pkgs;
            pkgs.mkShell {
              src = ./.;
              buildInputs = self.packages.${system}.default.buildInputs;
              nativeBuildInputs = with pkgs; self.packages.${system}.default.nativeBuildInputs ++ [ poppler_utils run check clippy (python3.withPackages (ps: with ps; [requests])) ];
            };
          apps.default = {
            type = "app";
            inherit name;
            program = "${self.packages.${system}.default}/bin/${name}";
          };
        })
    );
}
