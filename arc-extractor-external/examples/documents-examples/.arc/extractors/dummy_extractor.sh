#!/usr/bin/env sh

content=$(cat $ARC_CURRENT_DOCUMENT)
title=$(echo -n "$content" | head -n 1)

echo -n "$content" > $ARC_CURRENT_TEXT

echo "{\"title\": \""$title"\"}"
