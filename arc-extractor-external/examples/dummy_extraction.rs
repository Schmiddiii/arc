use arc_core::{ExtractionInput, Extractor, InitializationConfig};
use arc_extractor_external::ExternalExtractor;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let config = InitializationConfig::from_env().expect("Documents directory not set");
    let extractor = ExternalExtractor::new(config.clone(), "dummy_extractor.sh");
    let document = ExtractionInput {
        id: 1,
        slug: "document".into(),
        file_extension: "txt".to_string(),
        mime_type: mime::TEXT_PLAIN,
        config: Default::default(),
    };
    let result = extractor.extract(document)?;
    println!("Result: ");
    println!("{:#?}", result);
    let content = std::fs::read(config.text_path(1))?;
    let content = String::from_utf8_lossy(&content);
    println!("Content:");
    println!("{}", content);
    Ok(())
}
