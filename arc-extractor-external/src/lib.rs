use std::{
    fmt::Display,
    io::Write,
    path::{Path, PathBuf},
    process::{Command, Stdio},
};

use arc_core::{
    ExtractionError, ExtractionOutput, Extractor, InitializationConfig, ENV_CURRENT_DOCUMENT,
    ENV_CURRENT_TEXT, ENV_DOCUMENTS,
};

pub struct ExternalExtractor {
    command: PathBuf,
    config: InitializationConfig,
}

impl ExternalExtractor {
    pub fn new<P: AsRef<Path>>(config: InitializationConfig, command: P) -> Self {
        log::debug!(
            "Setting up an external extractor for the command: {}.",
            command.as_ref().display()
        );
        Self {
            config,
            command: command.as_ref().to_owned(),
        }
    }
}

#[derive(Debug)]
pub enum ExternalExtractorError {
    Io(std::io::Error),
    Serialize(serde_json::Error),
    OutputNoJson(String, serde_json::Error),
    ExtractorExitedFailure(i32),
}

impl Display for ExternalExtractorError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(e) => write!(f, "spawing an external extractor failed: {e}")?,
            Self::Serialize(e) => write!(f, "serializing the extractor input failed: {e}")?,
            Self::OutputNoJson(s, e) => write!(f, "the data that was returned by an external extractor was no JSON:\n{s}\nFailure due to: {e}")?,
            ExternalExtractorError::ExtractorExitedFailure(e) => write!(f, "the external extractor did not exit cleanly. Exit code: {e}")?,
        }
        Ok(())
    }
}

impl std::error::Error for ExternalExtractorError {}

impl From<std::io::Error> for ExternalExtractorError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<ExternalExtractorError> for ExtractionError {
    fn from(value: ExternalExtractorError) -> Self {
        ExtractionError::ExtractorError(Box::new(value))
    }
}

impl Extractor for ExternalExtractor {
    fn extract(
        &self,
        document: arc_core::ExtractionInput,
    ) -> Result<arc_core::ExtractionOutput, arc_core::ExtractionError> {
        log::debug!(
            "Calling external extractor command: {}.",
            self.command.display()
        );
        log::trace!("Building the command");
        let mut command = Command::new(self.config.extractor_path(&self.command));
        // Current dir
        log::trace!("Setting the current directory");
        command.current_dir(self.config.store_path());
        // Env variables
        log::trace!("Setting environmental variables.");

        let store_path = self.config.store_path();
        log::trace!("    - {} = {}", ENV_DOCUMENTS, store_path.display());
        command.env(ENV_DOCUMENTS, store_path);

        let current_document_path = self
            .config
            .document_path(document.id, &document.file_extension);
        log::trace!(
            "    - {} = {}",
            ENV_CURRENT_DOCUMENT,
            current_document_path.display()
        );
        command.env(ENV_CURRENT_DOCUMENT, current_document_path);
        let current_text_path = self.config.text_path(document.id);
        log::trace!(
            "    - {} = {}",
            ENV_CURRENT_TEXT,
            current_text_path.display()
        );
        command.env(ENV_CURRENT_TEXT, current_text_path);
        // Pipes
        log::trace!("Setting stdin and stdout pipes.");
        command.stdin(Stdio::piped());
        command.stdout(Stdio::piped());

        log::debug!("Running external command.");
        let mut child = command.spawn().map_err(|e| {
            ExtractionError::ExtractorError(Box::new(ExternalExtractorError::from(e)))
        })?;
        // TODO: When is stdin not available?
        if let Some(stdin) = &mut child.stdin {
            stdin
                .write(
                    serde_json::to_string(&document)
                        .map_err(|e| {
                            ExtractionError::ExtractorError(Box::new(
                                ExternalExtractorError::Serialize(e),
                            ))
                        })?
                        .as_bytes(),
                )
                .map_err(|e| {
                    ExtractionError::ExtractorError(Box::new(ExternalExtractorError::Io(e)))
                })?;
        }
        let result = child.wait_with_output().map_err(|e| {
            ExtractionError::ExtractorError(Box::new(ExternalExtractorError::from(e)))
        })?;

        if !result.status.success() {
            log::error!(
                "The external command did not exit cleanly: Exit code {}.",
                result.status.code().unwrap_or(-1)
            );
            return Err(ExternalExtractorError::ExtractorExitedFailure(
                result.status.code().unwrap_or(-1),
            )
            .into());
        }

        log::trace!(
            "Parsing output of the command: {}",
            String::from_utf8_lossy(&result.stdout)
        );
        let result: ExtractionOutput = serde_json::from_slice(&result.stdout).map_err(|e| {
            ExternalExtractorError::OutputNoJson(
                String::from_utf8_lossy(&result.stdout).to_string(),
                e,
            )
        })?;
        log::trace!("Parsed output of the command to: {:#?}.", result);

        Ok(result)
    }
}
