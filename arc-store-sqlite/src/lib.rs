mod models;
mod schema;

use std::{
    fmt::Display,
    ops::DerefMut,
    panic::AssertUnwindSafe,
    path::PathBuf,
    sync::{Arc, Mutex, MutexGuard},
};

use arc_core::{Document, DocumentId, InitializationConfig, Slug, Store};

use diesel::{
    Connection, ConnectionError, ExpressionMethods, OptionalExtension, QueryDsl, RunQueryDsl,
    SqliteConnection,
};
use diesel_migrations::{embed_migrations, EmbeddedMigrations, MigrationHarness};

pub const MIGRATIONS: EmbeddedMigrations = embed_migrations!("./migrations/");

#[derive(Debug)]
pub enum UnexpectedReturn {
    Size(usize, usize),
    Mime(String),
}

#[derive(Debug)]
pub enum SqliteStoreError {
    PathInvalid(PathBuf),
    ConnectionError(ConnectionError),
    Migration(Box<dyn std::error::Error + Send + Sync>),
    Db(diesel::result::Error),
    UnexpectedReturnData(UnexpectedReturn),
}

impl Display for SqliteStoreError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::PathInvalid(p) => {
                write!(f, "the path to the store was invalid: {}", p.display())?
            }
            Self::ConnectionError(c) => {
                write!(f, "failed to initialize a database connection: {c}")?
            }
            Self::Migration(e) => write!(f, "failed to migrate the database: {}", e)?,
            Self::Db(e) => write!(f, "the database threw an error: {}", e)?,
            Self::UnexpectedReturnData(UnexpectedReturn::Size(expected, got)) => {
                write!(f, "the database returned something with unexpected length: expected {expected} but got {got}")?
            }
            Self::UnexpectedReturnData(UnexpectedReturn::Mime(got)) => {
                write!(f, "the database returned something that should have been a mime type: got {got}")?
            }
        }
        Ok(())
    }
}

impl std::error::Error for SqliteStoreError {}

impl From<ConnectionError> for SqliteStoreError {
    fn from(value: ConnectionError) -> Self {
        Self::ConnectionError(value)
    }
}

impl From<diesel::result::Error> for SqliteStoreError {
    fn from(value: diesel::result::Error) -> Self {
        Self::Db(value)
    }
}

#[derive(Clone)]
pub struct SqliteStore {
    connection: Arc<AssertUnwindSafe<Mutex<SqliteConnection>>>,
}

impl SqliteStore {
    fn run_migration(&self) -> Result<(), <Self as Store>::Error> {
        log::debug!("Running migrations.");
        self.connection()
            .deref_mut()
            .run_pending_migrations(MIGRATIONS)
            .map_err(SqliteStoreError::Migration)?;
        Ok(())
    }

    pub(crate) fn connection(&self) -> MutexGuard<'_, SqliteConnection> {
        log::trace!("Getting access to the sqlite connection.");
        self.connection.lock().expect("storage is alive")
    }

    #[cfg(test)]
    pub fn memory() -> Result<Self, SqliteStoreError> {
        log::debug!("Creating the sqlite store in memory.");
        let connection = SqliteConnection::establish(":memory:")?;

        let s = Self {
            connection: Arc::new(AssertUnwindSafe(Mutex::new(connection))),
        };

        s.run_migration()?;

        Ok(s)
    }
}

impl Store for SqliteStore {
    type Error = SqliteStoreError;

    fn from_initialization_config(
        config: InitializationConfig,
    ) -> Result<Self, <Self as Store>::Error> {
        log::debug!("Creating the sqlite store from the initialization configuration.");
        let db_path = config.dot_arc_path().join("db");
        let connection = SqliteConnection::establish(
            db_path
                .clone()
                .to_str()
                .ok_or(SqliteStoreError::PathInvalid(db_path))?,
        )?;

        let s = Self {
            connection: Arc::new(AssertUnwindSafe(Mutex::new(connection))),
        };

        s.run_migration()?;

        Ok(s)
    }

    fn create_from_slug(&self, s: &Slug) -> Result<u64, <Self as Store>::Error> {
        log::debug!("Creating a document from a slug: {}", s.as_ref());
        use crate::schema::documents::dsl::*;
        let new_ids: Vec<i32> = diesel::insert_into(documents)
            .values((slug.eq(s.as_ref()),))
            .returning(id)
            .get_results(&mut *self.connection())?;
        if new_ids.len() != 1 {
            log::error!("Expected the DB to return the ID of the new document, but more or less was provided: {:?}.", new_ids);
            return Err(SqliteStoreError::UnexpectedReturnData(
                UnexpectedReturn::Size(1, new_ids.len()),
            ));
        }
        log::trace!("Giving back ID of new document: {}", new_ids[0]);
        Ok(new_ids[0] as u64)
    }

    fn set(&self, d: &Document) -> Result<(), <Self as Store>::Error> {
        use crate::schema::documents::dsl::*;
        use crate::schema::key_values::dsl::*;
        use crate::schema::tags::dsl::*;
        log::debug!("Setting a document: {:#?}.", d);

        let did = d.id as i32;

        let new_document = models::Document {
            id: did,
            slug: d.slug.clone().into(),
            file_extension: Some(d.file_extension.clone()),
            mime_type: Some(d.mime_type.to_string()),
            title: Some(d.title.clone()),
        };
        let new_tags: Vec<models::Tag> = d
            .tags
            .iter()
            .map(|t| models::Tag {
                id: did,
                tag: t.to_string().clone(),
            })
            .collect();
        let new_key_values: Vec<models::KeyValue> = d
            .data
            .iter()
            .map(|(k, v)| models::KeyValue {
                id: did,
                key_: k.to_string().clone(),
                value: v.clone(),
            })
            .collect();

        self.connection().deref_mut().transaction(|conn| {
            log::trace!("Starting transaction");
            log::trace!("Saving document.");
            diesel::replace_into(documents)
                .values(&new_document)
                .execute(conn)?;

            log::trace!("Deleting old tags.");
            diesel::delete(tags)
                .filter(crate::schema::tags::dsl::id.eq(did))
                .execute(conn)?;

            log::trace!("Saving tags.");
            diesel::insert_into(tags).values(&new_tags).execute(conn)?;

            log::trace!("Deleting old key_values.");
            diesel::delete(key_values)
                .filter(crate::schema::key_values::dsl::id.eq(did))
                .execute(conn)?;

            log::trace!("Saving key_values.");
            diesel::insert_into(key_values)
                .values(&new_key_values)
                .execute(conn)?;
            Ok::<(), SqliteStoreError>(())
        })?;

        log::trace!("Inserting new document was successfull.");

        Ok(())
    }

    fn get(&self, did: DocumentId) -> Result<Option<Document>, Self::Error> {
        use crate::schema::documents::dsl::*;
        use crate::schema::key_values::dsl::*;
        use crate::schema::tags::dsl::*;

        log::debug!("Getting a document by id: {}.", did);

        // TODO: Transaction?
        log::trace!("Getting the base document.");
        let Some(new_document): Option<models::Document> = documents
            .find(did as i32)
            .first(&mut *self.connection())
            .optional()?
        else {
            return Ok(None);
        };
        log::trace!("Getting the tags.");
        let new_tags: Vec<models::Tag> = tags
            .filter(crate::schema::tags::dsl::id.eq(did as i32))
            .get_results(&mut *self.connection())?;
        log::trace!("Getting the key-values.");
        let new_key_values: Vec<models::KeyValue> = key_values
            .filter(crate::schema::key_values::dsl::id.eq(did as i32))
            .get_results(&mut *self.connection())?;

        log::trace!("Constructing the document.");
        let document = Document {
            id: did,
            slug: new_document.slug.into(),
            file_extension: new_document.file_extension.unwrap_or_default(),
            mime_type: new_document
                .mime_type
                .clone()
                .map(|m| {
                    m.parse().map_err(|_| {
                        SqliteStoreError::UnexpectedReturnData(UnexpectedReturn::Mime(m))
                    })
                })
                .transpose()?
                .unwrap_or(mime::APPLICATION_OCTET_STREAM),
            title: new_document.title.unwrap_or_default(),
            tags: new_tags.into_iter().map(|t| t.tag.into()).collect(),
            data: new_key_values
                .into_iter()
                .map(|k| (k.key_.into(), k.value))
                .collect(),
        };
        log::trace!("Returning document {:#?}.", document);
        Ok(Some(document))
    }

    fn get_latest(&self) -> Result<Option<Document>, Self::Error> {
        use crate::schema::documents::dsl::*;
        use crate::schema::key_values::dsl::*;
        use crate::schema::tags::dsl::*;

        log::debug!("Getting the latest document.");

        // TODO: Transaction?
        log::trace!("Getting the base document.");
        let Some(new_document): Option<models::Document> = documents
            .order_by(crate::schema::documents::dsl::id.desc())
            .first(&mut *self.connection())
            .optional()?
        else {
            return Ok(None);
        };
        log::trace!("Getting the tags.");
        let new_tags: Vec<models::Tag> = tags
            .filter(crate::schema::tags::dsl::id.eq(new_document.id))
            .get_results(&mut *self.connection())?;
        log::trace!("Getting the key-values.");
        let new_key_values: Vec<models::KeyValue> = key_values
            .filter(crate::schema::key_values::dsl::id.eq(new_document.id))
            .get_results(&mut *self.connection())?;

        log::trace!("Constructing the document.");
        let document = Document {
            id: new_document.id as u64,
            slug: new_document.slug.into(),
            file_extension: new_document.file_extension.unwrap_or_default(),
            mime_type: new_document
                .mime_type
                .clone()
                .map(|m| {
                    m.parse().map_err(|_| {
                        SqliteStoreError::UnexpectedReturnData(UnexpectedReturn::Mime(m))
                    })
                })
                .transpose()?
                .unwrap_or(mime::APPLICATION_OCTET_STREAM),
            title: new_document.title.unwrap_or_default(),
            tags: new_tags.into_iter().map(|t| t.tag.into()).collect(),
            data: new_key_values
                .into_iter()
                .map(|k| (k.key_.into(), k.value))
                .collect(),
        };
        log::trace!("Returning document {:#?}.", document);
        Ok(Some(document))
    }

    fn get_all_ids(&self) -> Result<Vec<DocumentId>, Self::Error> {
        use crate::schema::documents::dsl::*;

        log::trace!("Getting all document IDs.");
        Ok(documents
            .select(crate::schema::documents::dsl::id)
            .get_results(&mut *self.connection())?
            .into_iter()
            .map(|i: i32| i as u64)
            .collect())
    }
}

#[cfg(test)]
mod test {
    use std::collections::{HashMap, HashSet};

    use diesel::RunQueryDsl;

    use super::*;
    use crate::models;

    #[test]
    fn store_migration_creates_documents() -> Result<(), Box<dyn std::error::Error>> {
        use crate::schema::documents::dsl::*;
        let store = SqliteStore::memory()?;
        let mut connection = store.connection();
        let d: Vec<models::Document> = documents.load(&mut *connection)?;
        assert_eq!(d.len(), 0);
        Ok(())
    }

    #[test]
    fn store_create_document_increments_ids() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let id1 = store.create_from_slug(&Slug::new("slug1"))?;
        let id2 = store.create_from_slug(&Slug::new("slug2"))?;
        let id3 = store.create_from_slug(&Slug::new("slug3"))?;
        assert_eq!(id1, 1);
        assert_eq!(id2, 2);
        assert_eq!(id3, 3);
        Ok(())
    }

    #[test]
    fn store_set_get_document() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document = Document {
            id: 10,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        store.set(&document)?;
        assert_eq!(Some(document), store.get(10)?);
        Ok(())
    }

    #[test]
    fn store_set_set_get_document() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document = Document {
            id: 10,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        store.set(&document)?;
        store.set(&document)?;
        assert_eq!(Some(document), store.get(10)?);
        Ok(())
    }

    #[test]
    fn store_set_set2_get_document() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 10,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 10,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        assert_eq!(Some(document2), store.get(10)?);
        Ok(())
    }

    #[test]
    fn store_get_latest() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 1,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 2,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        assert_eq!(Some(document2), store.get_latest()?);
        Ok(())
    }

    #[test]
    fn store_get_all_ids() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 1,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 2,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        let document5 = Document {
            id: 5,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        store.set(&document5)?;
        let all = store.get_all_ids()?;
        assert_eq!(vec![1, 2, 5], all);
        Ok(())
    }

    #[test]
    fn store_get_all() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 1,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 2,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        let document5 = Document {
            id: 5,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        store.set(&document5)?;
        let all = store.get_all()?;
        assert_eq!(vec![document1, document2, document5], all);
        Ok(())
    }

    #[test]
    fn search_nothing_is_all() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 1,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 2,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        let document5 = Document {
            id: 5,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        store.set(&document5)?;
        let all = store.search("", &HashSet::new(), &HashMap::new())?;
        assert_eq!(vec![document1, document2, document5], all);
        Ok(())
    }

    #[test]
    fn search_tags() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 1,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 2,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        let document5 = Document {
            id: 5,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        store.set(&document5)?;
        let all = store.search("", &HashSet::from(["tag21".into()]), &HashMap::new())?;
        assert_eq!(vec![document2, document5], all);
        Ok(())
    }

    #[test]
    fn search_data() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 1,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 2,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        let document5 = Document {
            id: 5,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        store.set(&document5)?;
        let all = store.search(
            "",
            &HashSet::from([]),
            &HashMap::from([("key21".into(), "value21".to_owned())]),
        )?;
        assert_eq!(vec![document2, document5], all);
        Ok(())
    }

    #[test]
    fn search_data_invalid() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 1,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 2,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        let document5 = Document {
            id: 5,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21_invalid".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        store.set(&document5)?;
        let all = store.search(
            "",
            &HashSet::from([]),
            &HashMap::from([("key21".into(), "value21".to_owned())]),
        )?;
        assert_eq!(vec![document2], all);
        Ok(())
    }

    #[test]
    fn search_nothing_found() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 1,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 2,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        let document5 = Document {
            id: 5,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        store.set(&document5)?;
        let all = store.search(
            "",
            &HashSet::from(["does-not-exist".into()]),
            &HashMap::from([]),
        )?;
        assert_eq!(Vec::<Document>::new(), all);
        Ok(())
    }

    #[test]
    fn search_title() -> Result<(), Box<dyn std::error::Error>> {
        let store = SqliteStore::memory()?;
        let document1 = Document {
            id: 1,
            slug: "some-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Title".to_owned(),
            tags: HashSet::from(["tag1".into(), "tag2".into(), "tag3".into()]),
            data: HashMap::from([
                ("key1".into(), "value1".to_owned()),
                ("key2".into(), "value2".to_owned()),
            ]),
        };
        let document2 = Document {
            id: 2,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Some Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        let document5 = Document {
            id: 5,
            slug: "some-other-document".to_owned().into(),
            file_extension: "pdf".to_owned(),
            mime_type: mime::APPLICATION_PDF,
            title: "Other Title".to_owned(),
            tags: HashSet::from(["tag21".into(), "tag22".into(), "tag23".into()]),
            data: HashMap::from([
                ("key21".into(), "value21".to_owned()),
                ("key22".into(), "value22".to_owned()),
            ]),
        };
        store.set(&document1)?;
        store.set(&document2)?;
        store.set(&document5)?;
        let result = store.search("Some", &HashSet::from([]), &HashMap::from([]))?;
        assert_eq!(vec![document1, document2], result);
        Ok(())
    }
}
