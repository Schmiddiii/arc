// @generated automatically by Diesel CLI.

diesel::table! {
    documents (id) {
        id -> Integer,
        slug -> Text,
        file_extension -> Nullable<Text>,
        mime_type -> Nullable<Text>,
        title -> Nullable<Text>,
    }
}

diesel::table! {
    key_values (id, value) {
        id -> Integer,
        key_ -> Text,
        value -> Text,
    }
}

diesel::table! {
    tags (id, tag) {
        id -> Integer,
        tag -> Text,
    }
}

diesel::joinable!(key_values -> documents (id));
diesel::joinable!(tags -> documents (id));

diesel::allow_tables_to_appear_in_same_query!(
    documents,
    key_values,
    tags,
);
