use diesel::prelude::*;

#[derive(Insertable, Queryable, Selectable, Debug)]
#[diesel(table_name = crate::schema::documents)]
#[diesel(check_for_backend(diesel::sqlite::Sqlite))]
pub struct Document {
    // TODO: Only supports "limited" number of documents.
    pub id: i32,
    pub slug: String,
    pub file_extension: Option<String>,
    pub mime_type: Option<String>,
    pub title: Option<String>,
}

#[derive(Insertable, Queryable, Selectable, Debug)]
#[diesel(table_name = crate::schema::tags)]
#[diesel(check_for_backend(diesel::sqlite::Sqlite))]
pub struct Tag {
    pub id: i32,
    pub tag: String,
}

#[derive(Insertable, Queryable, Selectable, Debug)]
#[diesel(table_name = crate::schema::key_values)]
#[diesel(check_for_backend(diesel::sqlite::Sqlite))]
pub struct KeyValue {
    pub id: i32,
    pub key_: String,
    pub value: String,
}
