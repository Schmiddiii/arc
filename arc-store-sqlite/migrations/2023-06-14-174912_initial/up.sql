CREATE TABLE documents (
    id INTEGER PRIMARY KEY NOT NULL,
    slug VARCHAR NOT NULL,
    file_extension VARCHAR,
    mime_type VARCHAR,
    title VARCHAR
);

CREATE TABLE tags (
    id INTEGER NOT NULL,
    tag VARCHAR NOT NULL,
    FOREIGN KEY(id) REFERENCES documents(id)
    PRIMARY KEY (id, tag)
);

CREATE TABLE key_values ( 
    id INTEGER NOT NULL,
    key_ VARCHAR NOT NULL,
    value VARCHAR NOT NULL,
    FOREIGN KEY(id) REFERENCES documents(id)
    PRIMARY KEY (id, value)
);