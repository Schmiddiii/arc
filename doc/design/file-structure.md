# File Structure

The file structure of arc looks as follows:

```
documents
    .arc
        config.toml
        db
        extractors
            custom-extractor1
            custom-extractor2
            ...
        taggers
            custom-tagger1
            custom-tagger2
            ...
        finalizers
            custom-finalizer1
            custom-finalizer2
        scripts
            custom-script1
            custom-script2
            ...
        documents
            1.pdf
            2.pdf
            3.mthml
            ...
        texts
            1.txt
            2.txt
            3.txt
            ...
```
