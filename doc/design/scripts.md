# Scripts

Scripts allow easy interaction of Arc with the "outside world". It can e.g. be used to extract documents outside of the store and into another program, or vice-versa.

## Input

A script can take an optional document. This document may be empty (e.g. when inserting a document from an external program into the store), but one can force giving a document from the configuration.
A script can, as with all other mechanisms, also take configuration.

### Example

The following may be the input to a script:

```json
{
    "id": 123,
    "slug": "some-slug",
    "file-extension": "pdf",
    "mime-type": "application/pdf",
    "title": "Automatically extracted Title",
    "tags": ["some-tag", "another-tag"],
    "data": {
        "key1": "value1",
        "key2": "value2"
    },
    "config": {
        "config-key-1": "config-value-1",
        "config-key-2": "config-value-2"
    }
}
```

## Output

All outputs of scripts will be ignored.

## Configuration

The following is an example configuration for a script:

```toml
[scripts]

[scripts."run-something"] # The list of scripts.
type = "something.py" # The type of script. This can either start with `builtin`, in which case a builtin script will be used, or otherwise a script from the corresponding directory will be used.
requires_document = true # If true, this script must be given an input document. If no input document is specified, the Arc will let the user search for a document.

[scripts."run-something".config] # Some configuration.
some-key = "some-value"
```

## TODO

- Currently, scripts without inputs is not yet implemented.
- Maybe in the future, scripts with output that may be inserted into the store could be interesting. This would require configuration for ignoring/capturing the output.
