# Builtin Taggers

Some taggers are already built-in. They can be accessed with their respective tagger names, which always start with `builtin/`.

## Extension (`builtin/extension`)

This just tags the file with its file extension.

## Keywords (`builtin/keywords`)

The keyword tagger tags a document based on its (text)-content. It can be configured per mime-type using the `tagger_config`, e.g.:

```toml
[mimes."application/pdf"]
extractor = ...
taggers = [ "builtin/keyword", ... ]

[mimes."application/pdf".tagger_config."builtin/keyword"]
"Top Secret" = "top-secret"
"Bottom Secret" = "bottom-secret"
```

### Notes

- The keyword tagger is case-sensitive.
- The keyword tagger is not smart regarding only tagging words, e.g. if it was asked to tag "Hi" with a tag, it will also tag e.g. "History".
- Currently, it is not possible to assign multiple tags for the same keyword.

## DateTime (`builtin/datetime`)

Adds the current date and time in format to the data of a document under the key "added".

You can configure the format of the output under the config value `format`. The default format is `%+` which corresponds to ISO 8601 / RFC 3339. Valid are all format parameters of chrono.

```toml

[mimes."application/pdf"]
extractor = ...
taggers = [ "builtin/datetime", ... ]

[mimes."application/pdf".tagger_config."builtin/datetime"]
"format" = "%F"
```

## MHTML Source (`builtin/mhtml/source`)

Tag the document with the source of the `mhtml`-file (the URL where it was extracted). This only works for `mhtml`-documents.

## TODO

- Link to chrono format parameters.
- Allow custom document data key for the datetime tagger.
