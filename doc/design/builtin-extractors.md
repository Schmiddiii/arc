# Builtin Extractors

Some extractors are already built-in. They can be accessed with their respective extractor names, which always start with `builtin/`.

## Pdf Extractor (`builtin/pdf`)

The PDF extractor is implemented using the `pdftotext`-tool of [`popper`](https://poppler.freedesktop.org/).

### Content Extraction

The content is extracted by a simple call to `pdftotext`.

### Title Extraction

The extraction of the title is a little bit harder. Currently the title extraction uses the `-tsv`-flag of `pdftotext` (this is specific to the `popper`-edition) to figure out the text and height of the text. The extractor then extracts the concurrent sequence of words that has the largest size and is on the first page.

This method has a few drawbacks:

- Having some big logo at the first page may choose that logo.
- Multiline titles are broken.

### Future

In the future, a more robust implementation using e.g. [lopdf](https://crates.io/crates/lopdf) may be desirable.


## MHTML (`builtin/mhtml`)

The MHTML extractor extracts content from [`mhtml`](https://en.wikipedia.org/wiki/MHTML).

### Content Extraction

The content is currently the first `html`-part of the `mhtml`-file extracted with the entire text.

### Title Extraction

The title is the subject of the `mhtml` file.
