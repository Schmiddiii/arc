# Document

A document is a file stored in the store.

## Format

A document consists of multiple entries. In JSON, the entries would look like:

```json
{
    "id": 123,
    "slug": "some-slug",
    "file-extension": "pdf",
    "mime-type": "application/pdf",
    "title": "Automatically extracted Title",
    "tags": ["some-tag", "another-tag"],
    "data": {
        "key1": "value1",
        "key2": "value2"
    }
}
```

The `title`, `tags` and `data` may be omitted. If omitted, they are assumed to be the corresponding empty value.
Furthermore, `tags` and the keys of `data` will be sluggified.
The actual file and text-content can be found in their respective sub-directories.

Note that for a file with two `.` (e.g. `some-file.tar.gz`), the `slug` will consist of everything up to the last dot (`some-file-tar`), the `file-extension` will be the part after the last `.` (`gz`).
