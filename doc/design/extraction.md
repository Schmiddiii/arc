# Extraction

Extraction is the process of extracting the `title` and `content` of a file.

## Input

The input to the extractor is the `slug` and `mime-type` of the application.
The extractor is guaranteed that at point of extraction, the document is already copied to the correct location in the arc store.
Furthermore, the text path it has to extract contents to does not exist. 
Note that these guarantees are not given if other programs (e.g. a parallel instance of Arc) modifies the store location.

### Example

The following may be the input to an extractor:

```json
{
    "id": 123,
    "slug": "some-file",
    "file-extension": "pdf",
    "mime-type": "application/pdf",
    "config": {
        "config-key-1": "config-value-1",
        "config-key-2": "config-value-2"
    }
}
```

## Processing

The extractor should now use that information in addition to the known archive directory to extract the contents and title of the file.

## Output

The extractor should output the extracted information as follows:


```json
{
    "title": "Some extracted Title",
}
```

Furthermore, it should output the text-content into the file at the path `$ARC_CURRENT_TEXT`. This file will not yet exist.

## TODO

- Explain error
- Explain config
