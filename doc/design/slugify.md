# Slugify

The file-names added to your store will all be sluggified to have a somewhat uniform naming.

## Process

Sluggification will (for slugs used internally):

- Rename the filename to kebab-case.
- Remove all non-alphanumeric characters.

In addition to that, the file stored will also have the following:

- Keep the original file extension.
- Prepend the id in front of the file name.

The file name as which a document is stored is therefore of the form `<id>-<slug>.<extension>`.

## Examples

Assume the current store includes the following documents:

```
1-important-work.pdf
2-another-file.pdf
3-website.mhtml
```

The following transformations will be done:

```
SomeNewFile.pdf -> 4-some-new-file.pdf
A File with Spaces.mhtml -> 5-a-file-with-spaces.mhtml
Website.mhtml -> 6-website.mhtml
```
