# Configuration

The configuration is a single `config.toml`-file stored in `.arc/config.toml`.

## Example

The following is an example configuration. Note that not all taggers/extractors/search filters are currently implemented or are planned to be implemented, this example only shows the structure.

```toml
[mimes]
# For each mime type, there is its own section.
[mimes."application/pdf"]
# Specify the extractor (from the list of extractors below).
extractor = "pdf"
# Specify the taggers (from the list of taggers below). This is similar to the extractors for the strings, there can be multiple taggers per file type given, they will be executed in the given order.
taggers = ["extension", "keywords", "custom-tagger"]
# Specify the finalizers (from the list of finalizers below). This is similar to the extractors for the strings, there can be multiple taggers per file type given, they will be executed in the given order.
finalizers = ["finalizing"]

# mhtml
[mimes."message/rfc822"]
extractor = "mhtml"
taggers = ["extension", "datetimetime", "source", "custom-other-tagger"]

[extractors]
# The list of extractors.
[extractors."pdf"]
# The type of extractor. This can either start with `builtin`, in which case a builtin extractor will be used, or otherwise a extractor from the corresponding directory will be used.
type = "builtin/pdf"

[extractors."mhtml"]
type = "builtin/mhtml"

[taggers]
# The list of taggers.
[taggers."extension"]
type = "builtin/extension"

[taggers."datetime"]
type = "builtin/datetime"

[taggers."keywords"]
type = "builtin/keywords"
[taggers."keywords".config]
# Configuration key-value for the extractor
"Top Secret" = "top-secret"
"Bottom Secret" = "bottom-secret"

[taggers."source"]
type = "builtin/mhtml/source"

# Examples of custom taggers which are not built-in.
[taggers."custom-tagger"]
type = "custom-other-tagger.py"
[taggers."custom-tagger"]
type = "custom-other-tagger.py"

[finalizers]
# The list of finalizers.
[finalizers."finalizing"]
type = "finalizing.py"

[scripts]
 # The list of scripts.
[scripts."leak-document"]
type = "leak_document.py"
requires_document = true # If true, this script must be given an input document. If no input document is specified, the Arc will let the user search for a document.

[scripts."leak_document".config] # Some configuration.
publish_on = "wikileaks"

# Custom filter: After a date.
[search.custom_filters."after"]
type = "builtin/time/after"

# Configuration for the custom filter.
[search.custom_filters."after".config]
document_key = "added"
document_format = "%+"
default_value = "yesterday"

[search.custom_filters."before"]
type = "builtin/time/before"
```

Note that the `extractors.*.config`, `taggers.*.config`, `finalizers.*.config` and `custom_filter."*".config` only allows a string-to-string map per extractor/tagger/finalizers/custom filter.

Note that `arc` does not care where the custom extractor is placed in. This may lead to arbitrary-code-execution if the content of the configuration can be modified.
