# Tagging

Tagging a document is the process of applying tags but also key-value data to a document. Note that multiple taggers may be run on one file, those outputs will be merged, the last tagger that was run takes precedence.

## Input

The input is the document without any tags and key-value data. Note that the `title` may be unset, e.g. if the extractor failed to extract any information from the document.
The tagger is guaranteed that the file with the slug was already copied in the well-known arc document location.

### Example

```json
{
    "id": 123,
    "slug": "some-file",
    "file-extension": "pdf",
    "mime-type": "application/pdf",
    "title": "Some extracted title that may be empty",
    "config": {
        "config-key-1": "config-value-1",
        "config-key-2": "config-value-2",
    }
}
```

It can furthermore use the file content at `$ARC_CURRENT_TEXT`. This file may not exist, e.g. if the extractor failed to extract any content from the document.

## Processing

The tagger should now inspect the document data that was given, but potentially also the file itself, to add tags and key-value data to the document.

## Output

The output should consist of the tags and key-value data that was added in the tagging run. Note that - as previously mentioned - multiple taggers may run for one file type, in which case the outputs will be merged. There is currently no way to remove tags that were applied in previous taggers. Also note that tags are deduplicated.

### Example

```json
{
    "tags": ["some-tag", "another-tag"],
    "data": {
        "key1": "value1",
        "key2": "value2"
    }
}
```

### Notes

- While it is possible for the keys of `data` to include `:`, this is highly discouraged as such data will break editing a document.

## TODO

- Explain error
- Explain config
