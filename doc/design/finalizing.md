# Finalizing

Finalizing is the process of taking the results of the extraction and tagging and do some final work on it. It may for example be used to implement a hierarchy of tags (if the document contains a tag, add some of the other tags) or renaming a file based on a tag (if the document contains a tag, then prepend something to the name).

## Input

The input of the finalizer is the finished document from extraction and tagging.
The finalizer is guaranteed that at point of extraction, the document is already copied to the correct location in the arc store. Furthermore, if it was done by the extractor, the corresponding text file is also set.

### Example

The following may be the input to an finalizer:

```json
{
    "id": 123,
    "slug": "some-slug",
    "file-extension": "pdf",
    "mime-type": "application/pdf",
    "title": "Automatically extracted Title",
    "tags": ["some-tag", "another-tag"],
    "data": {
        "key1": "value1",
        "key2": "value2"
    },
    "config": {
        "config-key-1": "config-value-1",
        "config-key-2": "config-value-2"
    }
}
```

## Processing

The finalizer should now process the input document and produce an output document.

## Output

The finalized should output the finalized information as a document without the `id`, `file-extension` or `mime-type`, e.g.:


```json
{
    "slug": "some-other-slug",
    "title": "Some changed title",
    "tags": ["finalized-some-tag", "finalized-another-tag"],
    "data": {
        "key1-final": "value1-final",
        "key2-final": "value2-final"
    }
}
```

Note that the finalizer can delete tags and data unlike the tagger.

## TODO

- Explain error
- Explain config
