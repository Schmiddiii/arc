# Search

The search is responsible for finding documents which:

- Contain a certain string in the title.
- Has certain tags and data.
- Contain a certain string in the text extracted.
- Match user-defined, custom filters.

The search happens in multiple steps:

- Query initial matching documents from the store. These documents returned are required to contain a certain string in the title and contain tags and data (for data, only equality of the data is checked).
- This list is filtered using user-defined scripts, e.g. to filter out documents in a certain timeframe.
- The even more reduced list will be full-text searched text.

## Search Interface

The search is given as a single string. This string contains all the information specified above. An example search includes:

```
Secrets - My top secret information #top-secret #classification-level:5 @later:yesterday
```

Breaking it down:

- This searches for documents containing "Secrets" in the title.
- The document has the text "My top secret information" in the document text.
- The document will have the tag `top-secret`.
- The document will have the data `classification-level` with the value `5`.
- A custom filter is called named `later` with the given argument `yesterday`.

### Notes

- The text can be mixed in with the tags/data/custom filters. This means that any word that is not part of a tag-, data- or custom filter is part of the text searched in the search, e.g. `Some #secret word` will search for `Some word` with tag `secret`.
- For tags/data/custom filters containing spaces, they will need to be quoted, e.g. `#"text with spaces"`. For data and custom filters, the two separate parts will need to be quoted, e.g. `#"Some Data":"Some Value"`.
- `:` in tags, data names or custom filter names must be quoted.
- Custom filters can also have no argument, you can leave out the `:` in that case.
- Unclosed `"` is interpreted as the tag/data/custom filter value going all the way to the end.
- Custom filters need to have sluggified name.

# TODO

- Registering custom search providers in the config.
- Excaping quotes, `#`, `@`.
- Ignores double spaces.
- Configuration for custom search.
