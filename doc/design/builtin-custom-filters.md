# Builtin Custom Filters

Some custom filters are already built-in. They can be accessed with their respective custom filter names, which always start with `builtin/`.

## Before (`builtin/time/before`)

This filters documents based on their insertion date. Only documents inserted before the date will be returned. Valid custom filter parameters are:

- `today`: Documents before (including) today.
- `yesterday`: Documents before (including) yesterday.
- `week`: Documents before (including) a week (7 days; note that this is different from "the start of this week").
- `month`: Documents before (including) one month (note that this is different from "the start of this month").
- `year`: Documents before (including) one year (12 months; note that this is different from "the start of this year").
- Most human-readable strings of dates (e.g. 2023-09-26).

### Configuration

This filter accepts the following configuration parameters:

- `document_key`: The key of the document data where the date is stored. Should be equivalent to the key defined in the datetime-tagger, and it defaults to `added` which is also the default for the datetime-tagger.
- `document_format`: The format how the date is stored under `document_key`. Should be equivalent to the format defined in the datetime tagger, and it defaults to `%+` which is also the default for the datetime-tagger.
- `default_value`: When the filter is not given any argument, what to assume as default. Defaults to `yesterday`.

### Notes

- If the argument cannot be parsed in any of the above mentioned parameters, it is assumend to be `yesterday`.

## After (`builtin/time/after`)

This is equivalent to the `builtin/time/before`-filter, but filters for documents after a date instead of before.

## TODO

- Link to chrono format parameters.
- Custom filters that are not built-in are currently not allowed.
