# Environmental Variables

## Set by the User

The user should set the following environmental variables:

- `ARC_DOCUMENTS`: The root directory of the arc document store.
- `ARC_CONFIG`: The path to the configuration file, if not given this is inferred by `ARC_DOCUMENTS`.

## Set by Arc

The following variables are set by `arc` for the extractors and taggers:

- `ARC_DOCUMENTS`: The root directory of the arc document store. (inherited)
- `ARC_CURRENT_DOCUMENT`: The file path to the current document. Alternatively, this can be constructed using `ARC_DOCUMENTS`, the file structure, the document id, the document slug and the document extension.
- `ARC_CURRENT_TEXT`: The file path to the current text-content of the document. Alternatively, this can be constructed using `ARC_DOCUMENTS`, the file structure, the document id, the document slug and the document extension.
