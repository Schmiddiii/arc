use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    // TODO: Utilize mime::Mime?
    pub mimes: HashMap<String, MimeConfig>,

    #[serde(default)]
    pub extractors: HashMap<String, ExtractorConfig>,
    #[serde(default)]
    pub taggers: HashMap<String, TaggerConfig>,
    #[serde(default)]
    pub finalizers: HashMap<String, FinalizerConfig>,
    #[serde(default)]
    pub scripts: HashMap<String, ScriptConfig>,

    #[serde(default)]
    pub search: SearchConfig,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MimeConfig {
    pub extractor: String,
    #[serde(default)]
    pub taggers: Vec<String>,
    #[serde(default)]
    pub finalizers: Vec<String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ExtractorConfig {
    pub r#type: String,
    #[serde(default)]
    pub config: HashMap<String, String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TaggerConfig {
    pub r#type: String,
    #[serde(default)]
    pub config: HashMap<String, String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct FinalizerConfig {
    pub r#type: String,
    #[serde(default)]
    pub config: HashMap<String, String>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ScriptConfig {
    pub r#type: String,
    #[serde(default)]
    pub requires_document: bool,
    #[serde(default)]
    pub config: HashMap<String, String>,
}

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
pub struct SearchConfig {
    // TODO: Maybe allow different search providers?
    // TODO: Configuration of search provider?
    #[serde(default)]
    pub provider: String,
    #[serde(default)]
    pub custom_filters: HashMap<String, SearchFilterConfig>,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct SearchFilterConfig {
    pub r#type: String,
    #[serde(default)]
    pub config: HashMap<String, String>,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn default_config_is_config() -> Result<(), Box<dyn std::error::Error>> {
        let config_file = include_str!("../../default_config.toml");
        let _: Config = toml::from_str(config_file)?;
        Ok(())
    }
}
