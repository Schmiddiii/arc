mod config;
use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
    fs::File,
    io::Write,
    path::Path,
};

use arc_script_external::ExternalScript;
use arc_util::{change_read_only, delete_if_exists};
pub use config::*;

use arc_core::{
    Document, ExtractionError, ExtractionInput, Extractor, Finalizer, FinalizerError,
    FinalizerInput, InitializationConfig, Script, ScriptError, ScriptInput, ScriptInputDocument,
    SearchCustomFilter, SearchProvider, SearchQuery, Slug, Store, Tagger, TaggerError, TaggerInput,
    UserInteraction,
};
use arc_extractor_builtin::MhtmlExtractor;
use arc_extractor_builtin::PdfExtractor;
use arc_extractor_external::ExternalExtractor;
use arc_finalizer_external::ExternalFinalizer;
use arc_search_custom_filters_builtin::{AfterTimeFilter, BeforeTimeFilter};
use arc_search_provider_native::NativeSearchProvider;
use arc_tagger_builtin::{DateTimeTagger, ExtensionTagger, KeywordTagger, MhtmlSourceTagger};
use arc_tagger_external::ExternalTagger;

const BUILTIN_START: &str = "builtin/";

pub struct Arc<S, I> {
    initialization_config: InitializationConfig,
    config: Config,
    store: S,
    interaction: I,
}

#[derive(Debug)]
pub enum ArcError<E, I> {
    Store(E),
    Interaction(I),
    Io(std::io::Error),
    Toml(toml::de::Error),
    StorePathDoesNotExist(std::path::PathBuf),
    PathHasNoFileStem(std::path::PathBuf),
    PathHasNoFileExtension(std::path::PathBuf),
    PathNoUtf8(std::path::PathBuf),
    PathDoesNotExist(std::path::PathBuf),
    NoConfigForMime(mime::Mime),
    ExtractionError(ExtractionError),
    TaggerError(TaggerError),
    FinalizerError(FinalizerError),
    ScriptError(ScriptError),
    CannotGuessMimeType(std::path::PathBuf),
    NoSuchBuiltInExtractor(String),
    NoSuchBuiltInTagger(String),
    NoSuchBuiltInFinalizer(String),
    NoSuchBuiltInScript(String),
    NoExtractor(String),
    NoTagger(String),
    NoFinalizer(String),
    NoScript(String),
}

impl<E: std::error::Error, I: std::error::Error> Display for ArcError<E, I> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Store(e) => write!(f, "the store failed: {e}")?,
            Self::Interaction(e) => write!(f, "user interaction failed: {e}")?,
            Self::Io(e) => write!(f, "error with the input/output: {e}")?,
            Self::Toml(e) => write!(f, "failed to parse toml file: {e}")?,
            Self::StorePathDoesNotExist(p) => {
                write!(f, "the store path does not exist: {}", p.display())?
            }
            Self::PathHasNoFileStem(p) => {
                write!(f, "the document path has no file stem: {}.", p.display())?
            }
            Self::PathHasNoFileExtension(p) => write!(
                f,
                "the document path has no file extension: {}.",
                p.display()
            )?,
            Self::PathNoUtf8(p) => write!(f, "the path is not valid UTF-8: {}.", p.display())?,
            Self::PathDoesNotExist(p) => write!(f, "the path does not exist: {}.", p.display())?,
            Self::NoConfigForMime(m) => {
                write!(f, "the mime type does not have a configuration: {m}")?
            }
            Self::ExtractionError(e) => write!(f, "the extractor failed: {}.", e)?,
            Self::TaggerError(e) => write!(f, "the tagger failed: {}.", e)?,
            Self::FinalizerError(e) => write!(f, "the finalizer failed: {}.", e)?,
            Self::ScriptError(e) => write!(f, "the script failed: {}.", e)?,
            Self::CannotGuessMimeType(p) => write!(
                f,
                "cannot guess the mime type for the file: {}.",
                p.display()
            )?,
            Self::NoSuchBuiltInExtractor(e) => {
                write!(f, "there is no builtin extractor for {}", e)?
            }
            Self::NoSuchBuiltInTagger(e) => write!(f, "there is no builtin tagger for {}", e)?,
            Self::NoSuchBuiltInFinalizer(e) => {
                write!(f, "there is no builtin finalizer for {}", e)?
            }
            Self::NoSuchBuiltInScript(e) => write!(f, "there is no builtin script for {}", e)?,
            Self::NoExtractor(e) => write!(f, "no extractor was defined for {}", e)?,
            Self::NoTagger(e) => write!(f, "no tagger was defined for {}", e)?,
            Self::NoFinalizer(e) => write!(f, "no finalizer was defined for {}", e)?,
            Self::NoScript(e) => write!(f, "no script was defined for {}", e)?,
        }
        Ok(())
    }
}

impl<E: std::error::Error, I: std::error::Error> std::error::Error for ArcError<E, I> {}

impl<E, I> From<std::io::Error> for ArcError<E, I> {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl<E, I> From<toml::de::Error> for ArcError<E, I> {
    fn from(value: toml::de::Error) -> Self {
        Self::Toml(value)
    }
}

impl<E, I> From<ExtractionError> for ArcError<E, I> {
    fn from(value: ExtractionError) -> Self {
        Self::ExtractionError(value)
    }
}

/// How to select a document (e.g. for editing, displaying).
pub enum DocumentSelection {
    /// Select the latest document that was inserted.
    Latest,
    /// Select a document by a slug. If multiple documents with the same slug exist, let the user choose one of them.
    // TODO: When failed, use search with the predefined slug?
    Slug(Slug),
    /// Let the user choose via an interactive search.
    Search,
}

impl<S: Store, I: UserInteraction> Arc<S, I> {
    pub fn new(
        initialization_config: InitializationConfig,
        interaction: I,
    ) -> Result<Self, ArcError<S::Error, I::Error>> {
        log::debug!(
            "Creating new Arc API from initialization config: {:#?}.",
            initialization_config
        );
        if !initialization_config.store_path().exists() {
            return Err(ArcError::StorePathDoesNotExist(
                initialization_config.store_path(),
            ));
        }
        initialization_config.setup_directory_structure()?;

        let config_path = initialization_config.config_path();
        if !initialization_config.config_path().exists() {
            log::debug!("The configuration does not yet exist. Creating the configuration.");
            let mut f = File::create(initialization_config.config_path())?;
            f.write_all(include_bytes!("../../default_config.toml"))?;
        }

        log::trace!("Reading the configuration file.");
        let config_file = std::fs::read_to_string(config_path).unwrap_or_default();
        log::trace!("Got configuration file: {}", config_file);
        let config: Config = toml::from_str(&config_file)?;
        log::info!("Running with configuration: {:#?}.", config);
        log::trace!("Initializing store.");
        let store = S::from_initialization_config(initialization_config.clone())
            .map_err(ArcError::Store)?;
        Ok(Self {
            initialization_config,
            config,
            store,
            interaction,
        })
    }

    pub fn add_document<P: AsRef<Path>>(
        &self,
        path: P,
        interactive: bool,
    ) -> Result<Document, ArcError<S::Error, I::Error>> {
        let path = path.as_ref();
        log::debug!("Adding document from path: {}.", path.display());

        if !path.exists() {
            log::error!(
                "The requested path for the new document does not exist: {}.",
                path.display()
            );
            return Err(ArcError::PathDoesNotExist(path.to_owned()));
        }

        log::trace!("Extracting file stem and extension information.");
        let file_stem =
            path.file_stem()
                .ok_or(ArcError::<S::Error, I::Error>::PathHasNoFileStem(
                    path.to_owned(),
                ))?;
        let file_extension =
            path.extension()
                .ok_or(ArcError::<S::Error, I::Error>::PathHasNoFileExtension(
                    path.to_owned(),
                ))?;
        log::trace!(
            "Got file stem: {}. Got file extension: {}.",
            file_stem.to_str().unwrap_or_default(),
            file_extension.to_str().unwrap_or_default()
        );
        log::trace!("Creating slug.");
        let slug: Slug = file_stem
            .to_str()
            .ok_or(ArcError::PathNoUtf8(path.to_owned()))?
            .into();
        log::trace!("Got slug: {}.", slug);
        log::trace!("Inserting document into store.");

        // Figure out mime type.
        // TODO: Maybe look at magic bytes?
        // TODO: This may not be stable.
        let mime_type = mime_guess::from_path(path)
            .first()
            .ok_or(ArcError::CannotGuessMimeType(path.to_path_buf()))?;
        let file_extension = file_extension.to_str().unwrap_or_default();

        log::trace!("Guessed mime type of document: {}", mime_type);

        let doc = self
            .store
            .create_from_basics(&slug, file_extension, mime_type)
            .map_err(ArcError::Store)?;
        log::trace!("Got ID for new document from store: {}.", doc.id);

        let document_path = self
            .initialization_config
            .document_path(doc.id, file_extension);

        // Copy file
        log::trace!(
            "Copying file: From {} to {}.",
            path.display(),
            document_path.display()
        );
        std::fs::copy(path, &document_path)?;

        // Change permissions to read-only.
        log::trace!("Making document file read-only.");
        change_read_only(&document_path, true)?;

        self.do_document(&doc, interactive)
    }

    fn find_extractor<STR: AsRef<str>>(
        &self,
        extractor: STR,
    ) -> Result<Box<dyn Extractor>, ArcError<S::Error, I::Error>> {
        let extractor = extractor.as_ref();
        log::trace!("Querying extractor for name: {}.", extractor);

        if let Some(extractor_rest) = extractor.strip_prefix(BUILTIN_START) {
            log::trace!("Extractor seems built-in.");
            return match extractor_rest {
                "pdf" => Ok(Box::new(PdfExtractor::new(
                    self.initialization_config.clone(),
                ))),
                "mhtml" => Ok(Box::new(MhtmlExtractor::new(
                    self.initialization_config.clone(),
                ))),
                _ => Err(ArcError::NoSuchBuiltInExtractor(extractor_rest.to_owned())),
            };
        }

        log::trace!("Extractor seems external.");
        let extractor =
            ExternalExtractor::new(self.initialization_config.clone(), Path::new(extractor));
        Ok(Box::new(extractor))
    }

    fn find_tagger<STR: AsRef<str>>(
        &self,
        tagger: STR,
    ) -> Result<Box<dyn Tagger>, ArcError<S::Error, I::Error>> {
        let tagger = tagger.as_ref();
        log::trace!("Querying tagger for name: {}.", tagger);

        if let Some(tagger_rest) = tagger.strip_prefix(BUILTIN_START) {
            log::trace!("Tagger seems built-in.");
            return match tagger_rest {
                "extension" => Ok(Box::new(ExtensionTagger::new())),
                "keywords" => Ok(Box::new(KeywordTagger::new(
                    self.initialization_config.clone(),
                ))),
                "datetime" => Ok(Box::new(DateTimeTagger::new())),
                "mhtml/source" => Ok(Box::new(MhtmlSourceTagger::new(
                    self.initialization_config.clone(),
                ))),
                _ => Err(ArcError::NoSuchBuiltInTagger(tagger_rest.to_owned())),
            };
        }

        log::trace!("Tagger seems external.");
        let tagger = ExternalTagger::new(self.initialization_config.clone(), Path::new(tagger));
        Ok(Box::new(tagger))
    }

    fn find_finalizer<STR: AsRef<str>>(
        &self,
        finalizer: STR,
    ) -> Result<Box<dyn Finalizer>, ArcError<S::Error, I::Error>> {
        let finalizer = finalizer.as_ref();
        log::trace!("Querying finalizer for name: {}.", finalizer);

        if let Some(finalizer_rest) = finalizer.strip_prefix(BUILTIN_START) {
            log::trace!("Finalizer seems built-in.");
            return Err(ArcError::NoSuchBuiltInFinalizer(finalizer_rest.to_owned()));
        }

        log::trace!("Finalizer seems external.");
        let tagger =
            ExternalFinalizer::new(self.initialization_config.clone(), Path::new(finalizer));
        Ok(Box::new(tagger))
    }

    pub fn modify(&self, document: Document) -> Result<(), ArcError<S::Error, I::Error>> {
        self.store.set(&document).map_err(ArcError::Store)?;
        Ok(())
    }

    pub fn latest(&self) -> Result<Option<Document>, ArcError<S::Error, I::Error>> {
        self.store.get_latest().map_err(ArcError::Store)
    }

    pub fn edit(
        &self,
        selection: &DocumentSelection,
    ) -> Result<Option<Document>, ArcError<S::Error, I::Error>> {
        let doc = self.document_from_selection(selection)?;
        if let Some(doc) = doc {
            let doc = self.interaction.edit(doc).map_err(ArcError::Interaction)?;
            self.modify(doc.clone())?;
            Ok(Some(doc))
        } else {
            Ok(None)
        }
    }

    pub fn show_all(&self) -> Result<(), ArcError<S::Error, I::Error>> {
        self.interaction
            .show_all(&self.store.get_all().map_err(ArcError::Store)?[..])
            .map_err(ArcError::Interaction)?;
        Ok(())
    }

    fn document_from_selection(
        &self,
        selection: &DocumentSelection,
    ) -> Result<Option<Document>, ArcError<S::Error, I::Error>> {
        Ok(match selection {
            DocumentSelection::Latest => self.latest()?,
            DocumentSelection::Slug(s) => {
                let by_slug = self.store.get_by_slug(s).map_err(ArcError::Store)?;
                match by_slug.len() {
                    0 => None,
                    1 => Some(by_slug[0].clone()),
                    // TODO: Search with given slug.
                    _ => self
                        .interaction
                        .search(|q| self.execute_search(&q).unwrap_or_default())
                        .map_err(ArcError::Interaction)?,
                }
            }
            DocumentSelection::Search => self
                .interaction
                .search(|q| self.execute_search(&q).unwrap_or_default())
                .map_err(ArcError::Interaction)?,
        })
    }

    pub fn open(&self, selection: &DocumentSelection) -> Result<(), ArcError<S::Error, I::Error>> {
        let doc = self.document_from_selection(selection)?;
        // TODO: Maybe error if no such document?
        if let Some(doc) = doc {
            self.interaction.open(&doc).map_err(ArcError::Interaction)?;
        }
        Ok(())
    }

    pub fn extract(
        &self,
        selection: &DocumentSelection,
    ) -> Result<(), ArcError<S::Error, I::Error>> {
        let doc = self.document_from_selection(selection)?;
        // TODO: Maybe error if no such document?
        if let Some(doc) = doc {
            std::fs::copy(
                self.initialization_config
                    .document_path(doc.id, &doc.file_extension),
                self.initialization_config
                    .extracted_document_path(doc.slug, &doc.file_extension),
            )?;
        }
        Ok(())
    }

    pub fn redo<P: AsRef<Path>>(
        &self,
        selection: &DocumentSelection,
        interactive: bool,
        path: Option<P>,
    ) -> Result<Option<Document>, ArcError<S::Error, I::Error>> {
        let doc = self.document_from_selection(selection)?;
        // TODO: Maybe error if no such document?
        if let Some(mut doc) = doc {
            if let Some(path) = path {
                // TODO: Refactor with add_document
                let path = path.as_ref();
                log::debug!("Re-Adding document from path: {}.", path.display());

                if !path.exists() {
                    log::error!(
                        "The requested path for the new document does not exist: {}.",
                        path.display()
                    );
                    return Err(ArcError::PathDoesNotExist(path.to_owned()));
                }

                log::trace!("Extracting file stem and extension information.");
                let file_stem =
                    path.file_stem()
                        .ok_or(ArcError::<S::Error, I::Error>::PathHasNoFileStem(
                            path.to_owned(),
                        ))?;
                let file_extension = path.extension().ok_or(
                    ArcError::<S::Error, I::Error>::PathHasNoFileExtension(path.to_owned()),
                )?;
                log::trace!(
                    "Got file stem: {}. Got file extension: {}.",
                    file_stem.to_str().unwrap_or_default(),
                    file_extension.to_str().unwrap_or_default()
                );
                log::trace!("Creating slug.");
                let slug: Slug = file_stem
                    .to_str()
                    .ok_or(ArcError::PathNoUtf8(path.to_owned()))?
                    .into();
                log::trace!("Got slug: {}.", slug);
                log::trace!("Inserting document into store.");

                // Figure out mime type.
                // TODO: Maybe look at magic bytes?
                // TODO: This may not be stable.
                let mime_type = mime_guess::from_path(path)
                    .first()
                    .ok_or(ArcError::CannotGuessMimeType(path.to_path_buf()))?;
                log::trace!("Guessed mime type of document: {}", mime_type);
                let file_extension = file_extension.to_str().unwrap_or_default();

                doc.file_extension = file_extension.to_owned();
                doc.slug = slug;
                doc.mime_type = mime_type;

                let document_path = self
                    .initialization_config
                    .document_path(doc.id, file_extension);
                let text_path = self.initialization_config.text_path(doc.id);

                log::trace!("Removing old files");
                change_read_only(&document_path, false)?;
                change_read_only(&text_path, false)?;
                std::fs::remove_file(&document_path)?;
                std::fs::remove_file(&text_path)?;

                // Copy file
                log::trace!(
                    "Copying file: From {} to {}.",
                    path.display(),
                    document_path.display()
                );
                std::fs::copy(path, &document_path)?;

                // Change permissions to read-only.
                log::trace!("Making document file read-only.");
                change_read_only(&document_path, true)?;

                Ok(Some(self.do_document(&doc, interactive)?))
            } else {
                Ok(Some(self.do_document(&doc, interactive)?))
            }
        } else {
            Ok(None)
        }
    }

    pub fn do_document(
        &self,
        doc: &Document,
        interactive: bool,
    ) -> Result<Document, ArcError<S::Error, I::Error>> {
        let id = doc.id;
        let slug = doc.slug.clone();
        let file_extension = doc.file_extension.to_owned();
        let mime_type = doc.mime_type.clone();

        let text_path = self.initialization_config.text_path(id);
        log::trace!("Deleting text-path if it exists: {}.", text_path.display());
        delete_if_exists(&text_path)?;

        let mime_config = self
            .config
            .mimes
            .get(&mime_type.to_string())
            .ok_or(ArcError::NoConfigForMime(mime_type.clone()))?;
        let extractor_config = self
            .config
            .extractors
            .get(&mime_config.extractor)
            .ok_or(ArcError::NoExtractor(mime_config.extractor.clone()))?;
        log::trace!("Loaded configuration for mime-type: {:#?}", mime_config);

        let extractor_input = ExtractionInput {
            id,
            slug: slug.clone(),
            file_extension: file_extension.to_owned(),
            mime_type: mime_type.clone(),
            config: extractor_config.config.clone(),
        };
        log::trace!("Constructed input for extractor: {:#?}.", extractor_input);
        let extractor = self.find_extractor(&extractor_config.r#type)?;
        log::trace!("Running extractor for input: {:#?}.", extractor_input);
        let extractor_output = extractor.extract(extractor_input)?;
        log::trace!("Got extractor output: {:#?}.", extractor_output);

        log::trace!("Making text-file read-only: {}.", text_path.display());
        change_read_only(&text_path, true)?;

        let tagger_input = TaggerInput {
            id,
            slug: slug.clone(),
            file_extension: file_extension.to_owned(),
            mime_type: mime_type.clone(),
            title: extractor_output.title.clone(),
            config: Default::default(),
        };

        let mut tags = HashSet::new();
        let mut data = HashMap::new();
        log::trace!("Applying taggers");
        for tagger_name in &mime_config.taggers {
            log::trace!("Running tagger {tagger_name}.");
            let tagger_config = self
                .config
                .taggers
                .get(tagger_name)
                .ok_or(ArcError::NoTagger(tagger_name.to_owned()))?;
            let tagger = self.find_tagger(&tagger_config.r#type)?;
            let mut tagger_input_specific = tagger_input.clone();
            tagger_input_specific.config = tagger_config.config.clone();
            let result = tagger
                .tag(tagger_input_specific)
                .map_err(ArcError::TaggerError)?;
            tags.extend(&mut result.tags.into_iter());
            data.extend(result.data.into_iter());
        }

        let mut document = Document {
            id,
            slug,
            file_extension,
            mime_type,
            title: extractor_output.title,
            tags,
            data,
        };

        log::trace!("Applying finalizers");
        for finalizer_name in &mime_config.finalizers {
            log::trace!("Running finalizer {finalizer_name}.");
            let finalizer_config = self
                .config
                .finalizers
                .get(finalizer_name)
                .ok_or(ArcError::NoFinalizer(finalizer_name.to_owned()))?;
            let finalizer = self.find_finalizer(&finalizer_config.r#type)?;
            let finalizer_input = FinalizerInput {
                id: document.id,
                slug: document.slug.clone(),
                file_extension: document.file_extension.clone(),
                mime_type: document.mime_type.clone(),
                title: document.title.clone(),
                tags: document.tags.clone(),
                data: document.data.clone(),
                config: finalizer_config.config.clone(),
            };
            let result = finalizer
                .finalize(finalizer_input)
                .map_err(ArcError::FinalizerError)?;

            document.slug = result.slug;
            document.title = result.title;
            document.tags = result.tags;
            document.data = result.data;
        }

        if interactive {
            document = self
                .interaction
                .edit(document)
                .map_err(ArcError::Interaction)?;
        }

        log::trace!("Constructed document from results: {:#?}.", document);
        self.store.set(&document).map_err(ArcError::Store)?;

        self.interaction
            .show(&document)
            .map_err(ArcError::Interaction)?;

        Ok(document)
    }

    fn execute_search(
        &self,
        query: &SearchQuery,
    ) -> Result<Vec<Document>, ArcError<S::Error, I::Error>> {
        // Search in the store.
        let mut results = self
            .store
            .search(&query.title, &query.tags, &query.data)
            .map_err(ArcError::Store)?;

        // Apply custom filters.
        for (key, arg) in &query.custom_filters {
            let Some(filter) = self.find_custom_filter(key) else {
                log::warn!("Did not find custom filter {}. Skipping.", key);
                continue;
            };
            results.retain(|d| filter.matches(d, arg.as_ref().map(|s| &s[..])));
        }

        // Apply full-text search.
        // If no text to search for, skip
        if !query.text.is_empty() {
            if let Some(provider) = self.find_search_provider(&self.config.search.provider) {
                results.retain(|d| provider.contains(d, &query.text));
            }
        }

        Ok(results)
    }

    fn find_custom_filter<STR: AsRef<str>>(
        &self,
        filter: STR,
    ) -> Option<Box<dyn SearchCustomFilter>> {
        let filter = filter.as_ref();
        log::trace!("Querying custom filter for name: {}.", filter);
        let Some(config) = self.config.search.custom_filters.get(filter) else {
            log::trace!("No custom filter found");
            return None;
        };

        if let Some(filter) = config.r#type.strip_prefix(BUILTIN_START) {
            log::trace!("filter seems built-in.");
            return match filter {
                "time/before" => Some(Box::new(BeforeTimeFilter::new(&config.config))),
                "time/after" => Some(Box::new(AfterTimeFilter::new(&config.config))),
                _ => {
                    log::warn!("Builtin custom filter not found: {}.", filter);
                    None
                }
            };
        }

        // TODO: External custom filters
        log::warn!("External custom filters are not yet allowed");
        None
    }

    fn find_search_provider<STR: AsRef<str>>(
        &self,
        provider: STR,
    ) -> Option<Box<dyn SearchProvider>> {
        let provider = provider.as_ref();
        log::trace!("Querying search provider for name: {}.", provider);

        if provider.is_empty() {
            // Use native provider as default if no value was given.
            return Some(Box::new(NativeSearchProvider::new(
                self.initialization_config.clone(),
            )));
        }

        if let Some(provider) = provider.strip_prefix(BUILTIN_START) {
            log::trace!("provider seems built-in.");
            return match provider {
                "native" => Some(Box::new(NativeSearchProvider::new(
                    self.initialization_config.clone(),
                ))),
                _ => {
                    log::warn!("Builtin provider not found: {}.", provider);
                    None
                }
            };
        }

        // TODO: External custom filters
        log::warn!("External search provider not yet implemented");
        None
    }

    fn find_script<STR: AsRef<str>>(
        &self,
        script: STR,
    ) -> Result<Box<dyn Script>, ArcError<S::Error, I::Error>> {
        let script = script.as_ref();
        log::trace!("Querying script for name: {}.", script);

        if let Some(script_rest) = script.strip_prefix(BUILTIN_START) {
            log::trace!("Script seems built-in.");
            return Err(ArcError::NoSuchBuiltInScript(script_rest.to_owned()));
        }

        log::trace!("Script seems external.");
        let script = ExternalScript::new(self.initialization_config.clone(), Path::new(script));
        Ok(Box::new(script))
    }

    pub fn script<STR: AsRef<str>>(
        &self,
        script: STR,
        selector: Option<&DocumentSelection>,
    ) -> Result<(), ArcError<S::Error, I::Error>> {
        let script = script.as_ref();
        let mut selector = selector;
        log::debug!("Running script {}", script);
        let script_config = &self
            .config
            .scripts
            .get(script)
            .ok_or(ArcError::NoScript(script.to_owned()))?;

        if script_config.requires_document && selector.is_none() {
            selector = Some(&DocumentSelection::Search);
        }

        // XXX: Maybe error?
        let document = if let Some(selector) = selector {
            self.document_from_selection(selector)?
        } else {
            None
        };

        let script_input = ScriptInput {
            document: document.map(|document| ScriptInputDocument {
                id: document.id,
                slug: document.slug.clone(),
                file_extension: document.file_extension.clone(),
                mime_type: document.mime_type.clone(),
                title: document.title.clone(),
                tags: document.tags.clone(),
                data: document.data.clone(),
            }),
            config: script_config.config.clone(),
        };

        let script = self.find_script(&script_config.r#type)?;
        script.run(script_input).map_err(ArcError::ScriptError)
    }
}

#[cfg(test)]
mod test {
    use std::cell::{Cell, RefCell};
    use std::convert::Infallible;
    use std::fs::OpenOptions;
    use std::os::unix::prelude::PermissionsExt;
    use std::rc::Rc;

    use super::*;
    use tempfile::tempdir;

    pub struct NoUserInteraction;

    impl UserInteraction for NoUserInteraction {
        type Error = Infallible;

        fn open(&self, _document: &crate::Document) -> Result<(), Self::Error> {
            Ok(())
        }

        fn show(&self, _document: &crate::Document) -> Result<(), Self::Error> {
            Ok(())
        }

        fn edit(&self, document: crate::Document) -> Result<crate::Document, Self::Error> {
            Ok(document)
        }

        fn show_all(&self, _documents: &[Document]) -> Result<(), Self::Error> {
            Ok(())
        }

        fn search<F: Fn(SearchQuery) -> Vec<Document>>(
            &self,
            _query: F,
        ) -> Result<Option<Document>, Self::Error> {
            Ok(None)
        }
    }

    #[test]
    fn api_creates_db() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        let _api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        assert!(config.dot_arc_path().join("db").exists());
        Ok(())
    }

    #[test]
    fn api_reads_db() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config1 = InitializationConfig::new(&tmp);
        let api1 = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config1,
            NoUserInteraction,
        )?;
        let id = api1.store.create_from_slug(&Slug::new("some-doc"))?;
        drop(api1);
        let config2 = InitializationConfig::new(&tmp);
        let api2 = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config2,
            NoUserInteraction,
        )?;
        assert!(api2.store.get(id)?.is_some());
        Ok(())
    }

    #[test]
    fn api_creates_config() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        let _api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        assert!(config.config_path().exists());
        Ok(())
    }

    #[test]
    fn api_reads_config() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        config.setup_directory_structure()?;
        let mut f = File::create(config.config_path())?;
        f.write_all(
            r#"
            [mimes]
            [mimes."application/pdf"]
            extractor = "something"
            taggers = []
            "#
            .as_bytes(),
        )?;
        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config,
            NoUserInteraction,
        )?;
        assert!(api.config.mimes.contains_key("application/pdf"));
        Ok(())
    }

    #[test]
    fn api_add_complaints_with_non_existing_path() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config,
            NoUserInteraction,
        )?;
        let result = api.add_document(tmp.path().join("this-does-not-exist.pdf"), false);
        assert!(matches!(result, Err(ArcError::PathDoesNotExist(_))));
        Ok(())
    }

    #[test]
    fn api_add_complaints_with_no_extension() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config,
            NoUserInteraction,
        )?;

        let file = tmp.path().join("this-will-be-created-but-has-no-extension");
        let _f = File::create(&file);

        let result = api.add_document(file, false);
        assert!(matches!(result, Err(ArcError::PathHasNoFileExtension(_))));
        Ok(())
    }

    #[test]
    fn api_add_works_with_normal_document() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        create_dummy_pdf_extractor(&config)?;
        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config,
            NoUserInteraction,
        )?;

        let file = tmp.path().join("this-will-be-created.pdf");
        let _f = File::create(&file);

        api.add_document(file, false)?;
        Ok(())
    }

    #[test]
    fn api_add_copies_document() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        create_dummy_pdf_extractor(&config)?;
        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;

        let file = tmp.path().join("this-will-be-created.pdf");
        let _f = File::create(&file);

        api.add_document(file, false)?;
        assert!(config.document_path(1, "pdf").exists());
        Ok(())
    }

    #[test]
    fn api_makes_read_only() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        create_dummy_pdf_extractor(&config)?;
        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;

        let file = tmp.path().join("this-will-be-created.pdf");
        let _f = File::create(&file);
        drop(_f);

        api.add_document(&file, false)?;

        let f = File::open(config.document_path(1, "pdf"))?;

        assert!(f.metadata()?.permissions().readonly());

        Ok(())
    }

    fn add_to_config(
        config: &InitializationConfig,
        s: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut config_file = OpenOptions::new()
            .append(true)
            .create(true)
            .open(config.config_path())?;
        config_file.write_all(s.as_bytes())?;
        Ok(())
    }

    fn create_extractor(
        config: &InitializationConfig,
        name: &str,
        content: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut extractor_file = File::create(config.extractor_path(Path::new(name)))?;
        extractor_file.write_all(content.as_bytes())?;
        let mut permissions = extractor_file.metadata()?.permissions();
        permissions.set_mode(0o777);
        extractor_file.set_permissions(permissions)?;

        add_to_config(
            config,
            &format!(
                r#"
                    [extractors."{}"]
                    type = "{}"
                "#,
                name, name
            ),
        )?;

        Ok(())
    }

    fn create_tagger(
        config: &InitializationConfig,
        name: &str,
        content: &str,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let mut tagger_file = File::create(config.tagger_path(Path::new(name)))?;
        tagger_file.write_all(content.as_bytes())?;
        let mut permissions = tagger_file.metadata()?.permissions();
        permissions.set_mode(0o777);
        tagger_file.set_permissions(permissions)?;

        add_to_config(
            config,
            &format!(
                r#"
                    [taggers."{}"]
                    type = "{}"
                "#,
                name, name
            ),
        )?;

        Ok(())
    }

    fn create_dummy_pdf_extractor(
        config: &InitializationConfig,
    ) -> Result<(), Box<dyn std::error::Error>> {
        config.setup_directory_structure()?;
        add_to_config(
            config,
            r#"
            [mimes."application/pdf"]
            extractor = "dummy-extractor"
            taggers = []
            "#,
        )?;

        create_extractor(
            config,
            "dummy-extractor",
            r#"#!/bin/sh
            echo '{"title": "Dummy"}'
            "#,
        )?;

        Ok(())
    }

    fn create_functional_pdf_extractor(
        config: &InitializationConfig,
    ) -> Result<(), Box<dyn std::error::Error>> {
        config.setup_directory_structure()?;

        add_to_config(
            config,
            r#"
            [mimes."application/pdf"]
            extractor = "my-extractor"
            taggers = ["my-tagger"]
            "#,
        )?;

        create_extractor(
            config,
            "my-extractor",
            r#"#!/bin/sh
            echo Some Content > $ARC_CURRENT_TEXT
            echo '{"title": "Some Title"}'
            "#,
        )?;

        create_tagger(
            config,
            "my-tagger",
            r#"#!/bin/sh
            echo '{"tags": ["Dummy1", "Dummy2"], "data": {"key1": "Value1", "key2": "Value2"}}'
            "#,
        )?;

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_runs_extractor() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file = tmp.path().join("this-will-be-created.pdf");
        let _f = File::create(&file);

        create_functional_pdf_extractor(&config)?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        api.add_document(&file, false)?;

        assert!(config.text_path(1).exists());

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_inserts_into_store() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file = tmp.path().join("this-will-be-created.pdf");
        let _f = File::create(&file);

        create_functional_pdf_extractor(&config)?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config,
            NoUserInteraction,
        )?;
        api.add_document(&file, false)?;

        assert!(api
            .store
            .get(1)?
            .is_some_and(|d| d.slug == "this-will-be-created".into()));

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_runs_tagger() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file = tmp.path().join("this-will-be-created.pdf");
        let _f = File::create(&file);

        create_functional_pdf_extractor(&config)?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        let document = api.add_document(&file, false)?;

        assert_eq!(
            document.tags,
            HashSet::from(["Dummy1".into(), "Dummy2".into()])
        );

        assert_eq!(
            document.data,
            HashMap::from([
                ("key1".into(), "Value1".to_owned()),
                ("key2".into(), "Value2".to_owned()),
            ])
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_document_selection_latest() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        create_functional_pdf_extractor(&config)?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        let _document = api.add_document(&file1, false)?;
        let document2 = api.add_document(&file2, false)?;

        assert_eq!(
            Some(document2),
            api.document_from_selection(&DocumentSelection::Latest)?
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_document_search_interactive_calls_user_interaction(
    ) -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        #[derive(Clone)]
        struct SearchUserInteraction {
            called: Rc<Cell<bool>>,
        }

        impl UserInteraction for SearchUserInteraction {
            type Error = Infallible;

            fn open(&self, _document: &Document) -> Result<(), Self::Error> {
                panic!()
            }

            fn show(&self, _document: &Document) -> Result<(), Self::Error> {
                // Called during creation of document, cannot panic.
                Ok(())
            }

            fn edit(&self, _document: Document) -> Result<Document, Self::Error> {
                panic!()
            }

            fn show_all(&self, _documents: &[Document]) -> Result<(), Self::Error> {
                panic!()
            }

            fn search<F: Fn(SearchQuery) -> Vec<Document>>(
                &self,
                query: F,
            ) -> Result<Option<Document>, Self::Error> {
                self.called.set(true);
                Ok(query(SearchQuery::default()).get(0).cloned())
            }
        }

        create_functional_pdf_extractor(&config)?;

        let ui = SearchUserInteraction {
            called: Rc::new(Cell::new(false)),
        };
        let api = Arc::<arc_store_sqlite::SqliteStore, SearchUserInteraction>::new(
            config.clone(),
            ui.clone(),
        )?;
        let document1 = api.add_document(&file1, false)?;
        let _document2 = api.add_document(&file2, false)?;

        assert_eq!(
            Some(&document1),
            api.document_from_selection(&DocumentSelection::Search)?
                .as_ref()
        );
        assert!(ui.called.get());

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_edit_calls_ui_edit() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        #[derive(Clone)]
        struct EditUserInterface {
            doc: Rc<RefCell<Option<Document>>>,
        }

        impl UserInteraction for EditUserInterface {
            type Error = Infallible;

            fn open(&self, _document: &Document) -> Result<(), Self::Error> {
                panic!()
            }

            fn show(&self, _document: &Document) -> Result<(), Self::Error> {
                // Called during creation of document, cannot panic.
                Ok(())
            }

            fn edit(&self, document: Document) -> Result<Document, Self::Error> {
                self.doc.replace(Some(document.clone()));
                let mut document = document;
                document.tags.insert("edited".into());
                Ok(document)
            }

            fn show_all(&self, _documents: &[Document]) -> Result<(), Self::Error> {
                panic!()
            }

            fn search<F: Fn(SearchQuery) -> Vec<Document>>(
                &self,
                _query: F,
            ) -> Result<Option<Document>, Self::Error> {
                panic!()
            }
        }

        create_functional_pdf_extractor(&config)?;

        let ui = EditUserInterface {
            doc: Rc::new(RefCell::new(None)),
        };
        let api = Arc::<arc_store_sqlite::SqliteStore, EditUserInterface>::new(
            config.clone(),
            ui.clone(),
        )?;
        let _document = api.add_document(&file1, false)?;
        let document2 = api.add_document(&file2, false)?;

        api.edit(&DocumentSelection::Latest)?;
        assert_eq!(Some(document2), (*ui.doc).clone().into_inner());
        assert!(api
            .latest()?
            .expect("At least one document to be inserted")
            .tags
            .contains(&"edited".into()));

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_open_calls_ui_open() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        #[derive(Clone)]
        struct OpenUserInterface {
            doc: Rc<RefCell<Option<Document>>>,
        }

        impl UserInteraction for OpenUserInterface {
            type Error = Infallible;

            fn open(&self, document: &Document) -> Result<(), Self::Error> {
                self.doc.replace(Some(document.clone()));
                Ok(())
            }

            fn show(&self, _document: &Document) -> Result<(), Self::Error> {
                // Called during creation of document, cannot panic.
                Ok(())
            }

            fn edit(&self, _document: Document) -> Result<Document, Self::Error> {
                panic!()
            }

            fn show_all(&self, _documents: &[Document]) -> Result<(), Self::Error> {
                panic!()
            }

            fn search<F: Fn(SearchQuery) -> Vec<Document>>(
                &self,
                _query: F,
            ) -> Result<Option<Document>, Self::Error> {
                panic!()
            }
        }

        create_functional_pdf_extractor(&config)?;

        let ui = OpenUserInterface {
            doc: Rc::new(RefCell::new(None)),
        };
        let api = Arc::<arc_store_sqlite::SqliteStore, OpenUserInterface>::new(
            config.clone(),
            ui.clone(),
        )?;
        let _document = api.add_document(&file1, false)?;
        let document2 = api.add_document(&file2, false)?;

        api.open(&DocumentSelection::Latest)?;
        assert_eq!(Some(document2), (*ui.doc).clone().into_inner());

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_show_all_calls_ui_show_all() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        #[derive(Clone)]
        struct ShowAllUserInterface {
            doc: Rc<RefCell<Vec<Document>>>,
        }

        impl UserInteraction for ShowAllUserInterface {
            type Error = Infallible;

            fn open(&self, _document: &Document) -> Result<(), Self::Error> {
                panic!()
            }

            fn show(&self, _document: &Document) -> Result<(), Self::Error> {
                // Called during creation of document, cannot panic.
                Ok(())
            }

            fn edit(&self, _document: Document) -> Result<Document, Self::Error> {
                panic!()
            }

            fn show_all(&self, documents: &[Document]) -> Result<(), Self::Error> {
                self.doc.replace(documents.to_vec());
                Ok(())
            }

            fn search<F: Fn(SearchQuery) -> Vec<Document>>(
                &self,
                _query: F,
            ) -> Result<Option<Document>, Self::Error> {
                panic!()
            }
        }

        create_functional_pdf_extractor(&config)?;

        let ui = ShowAllUserInterface {
            doc: Rc::new(RefCell::new(vec![])),
        };
        let api = Arc::<arc_store_sqlite::SqliteStore, ShowAllUserInterface>::new(
            config.clone(),
            ui.clone(),
        )?;
        let document1 = api.add_document(&file1, false)?;
        let document2 = api.add_document(&file2, false)?;

        api.show_all()?;
        assert_eq!(vec![document1, document2], (*ui.doc).clone().into_inner());

        Ok(())
    }

    #[test]
    fn api_modify_no_slug_change() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);
        create_dummy_pdf_extractor(&config)?;
        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;

        let file = tmp.path().join("this-will-be-created.pdf");
        let _f = File::create(&file);
        drop(_f);

        let mut doc = api.add_document(&file, false)?;

        doc.title = "This has been modified".to_string();
        doc.tags.insert("This has been modified".into());

        api.modify(doc.clone())?;

        let last = api.latest()?.expect("One document to be in the store");

        assert_eq!(doc, last);

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_document_selection_slug_single_directly_returns(
    ) -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        #[derive(Clone)]
        struct SelectorUserInteraction {}

        impl UserInteraction for SelectorUserInteraction {
            type Error = Infallible;

            fn open(&self, _document: &Document) -> Result<(), Self::Error> {
                panic!()
            }

            fn show(&self, _document: &Document) -> Result<(), Self::Error> {
                // Called during creation of document, cannot panic.
                Ok(())
            }

            fn edit(&self, _document: Document) -> Result<Document, Self::Error> {
                panic!()
            }

            fn show_all(&self, _documents: &[Document]) -> Result<(), Self::Error> {
                panic!()
            }

            fn search<F: Fn(SearchQuery) -> Vec<Document>>(
                &self,
                _query: F,
            ) -> Result<Option<Document>, Self::Error> {
                panic!()
            }
        }

        create_functional_pdf_extractor(&config)?;

        let ui = SelectorUserInteraction {};
        let api = Arc::<arc_store_sqlite::SqliteStore, SelectorUserInteraction>::new(
            config.clone(),
            ui.clone(),
        )?;
        let document1 = api.add_document(&file1, false)?;
        let _document2 = api.add_document(&file2, false)?;

        assert_eq!(
            Some(&document1),
            api.document_from_selection(&DocumentSelection::Slug("this-will-be-created-1".into()))?
                .as_ref()
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_document_selection_slug_not_existing() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        #[derive(Clone)]
        struct SelectorUserInteraction {}

        impl UserInteraction for SelectorUserInteraction {
            type Error = Infallible;

            fn open(&self, _document: &Document) -> Result<(), Self::Error> {
                panic!()
            }

            fn show(&self, _document: &Document) -> Result<(), Self::Error> {
                // Called during creation of document, cannot panic.
                Ok(())
            }

            fn edit(&self, _document: Document) -> Result<Document, Self::Error> {
                panic!()
            }

            fn show_all(&self, _documents: &[Document]) -> Result<(), Self::Error> {
                panic!()
            }

            fn search<F: Fn(SearchQuery) -> Vec<Document>>(
                &self,
                _query: F,
            ) -> Result<Option<Document>, Self::Error> {
                panic!()
            }
        }

        create_functional_pdf_extractor(&config)?;

        let ui = SelectorUserInteraction {};
        let api = Arc::<arc_store_sqlite::SqliteStore, SelectorUserInteraction>::new(
            config.clone(),
            ui.clone(),
        )?;
        let _document1 = api.add_document(&file1, false)?;
        let _document2 = api.add_document(&file2, false)?;

        assert_eq!(
            None,
            api.document_from_selection(&DocumentSelection::Slug("this-does-not-exist".into()))?
                .as_ref()
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_document_selection_slug_multiple_matches() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        #[derive(Clone)]
        struct SearchUserInteraction {
            called: Rc<Cell<bool>>,
        }

        impl UserInteraction for SearchUserInteraction {
            type Error = Infallible;

            fn open(&self, _document: &Document) -> Result<(), Self::Error> {
                panic!()
            }

            fn show(&self, _document: &Document) -> Result<(), Self::Error> {
                // Called during creation of document, cannot panic.
                Ok(())
            }

            fn edit(&self, _document: Document) -> Result<Document, Self::Error> {
                panic!()
            }

            fn show_all(&self, _documents: &[Document]) -> Result<(), Self::Error> {
                panic!()
            }

            fn search<F: Fn(SearchQuery) -> Vec<Document>>(
                &self,
                query: F,
            ) -> Result<Option<Document>, Self::Error> {
                self.called.set(true);
                Ok(query(SearchQuery::default()).get(1).cloned())
            }
        }

        create_functional_pdf_extractor(&config)?;

        let ui = SearchUserInteraction {
            called: Rc::new(Cell::new(false)),
        };
        let api = Arc::<arc_store_sqlite::SqliteStore, SearchUserInteraction>::new(
            config.clone(),
            ui.clone(),
        )?;
        let _document1_1 = api.add_document(&file1, false)?;
        let document1_2 = api.add_document(&file1, false)?;
        let _document2 = api.add_document(&file2, false)?;

        assert_eq!(
            Some(&document1_2),
            api.document_from_selection(&DocumentSelection::Slug("this-will-be-created-1".into()))?
                .as_ref()
        );
        assert!(ui.called.get());

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_search_all() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        create_functional_pdf_extractor(&config)?;

        add_to_config(
            &config,
            r#"[search.custom_filters."later"]
            type = "builtin/time/after""#,
        )?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        let document1_1 = api.add_document(&file1, false)?;
        let document1_2 = api.add_document(&file1, false)?;
        let document2 = api.add_document(&file2, false)?;

        assert_eq!(
            vec![document1_1, document1_2, document2],
            api.execute_search(&SearchQuery {
                ..Default::default()
            })?
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_search_filter_tag() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        create_functional_pdf_extractor(&config)?;

        add_to_config(
            &config,
            r#"[search.custom_filters."later"]
            type = "builtin/time/after""#,
        )?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        let _document1_1 = api.add_document(&file1, false)?;
        let mut document1_2 = api.add_document(&file1, false)?;
        let mut document2 = api.add_document(&file2, false)?;

        document2.tags.insert("tag".into());
        document1_2.tags.insert("tag".into());
        api.modify(document1_2.clone())?;
        api.modify(document2.clone())?;

        assert_eq!(
            vec![document1_2, document2],
            api.execute_search(&SearchQuery {
                tags: HashSet::from(["tag".into()]),
                ..Default::default()
            })?
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_search_filter_data() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        create_functional_pdf_extractor(&config)?;

        add_to_config(
            &config,
            r#"[search.custom_filters."later"]
            type = "builtin/time/after""#,
        )?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        let _document1_1 = api.add_document(&file1, false)?;
        let mut document1_2 = api.add_document(&file1, false)?;
        let mut document2 = api.add_document(&file2, false)?;

        document2.data.insert("key".into(), "value".into());
        document1_2.data.insert("key".into(), "value".to_owned());
        document1_2.tags.insert("tag".into());
        api.modify(document1_2.clone())?;
        api.modify(document2.clone())?;

        assert_eq!(
            vec![document1_2, document2],
            api.execute_search(&SearchQuery {
                data: HashMap::from([("key".into(), "value".to_owned())]),
                ..Default::default()
            })?
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_search_filter_tag_data_mix() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        create_functional_pdf_extractor(&config)?;

        add_to_config(
            &config,
            r#"[search.custom_filters."later"]
            type = "builtin/time/after""#,
        )?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        let _document1_1 = api.add_document(&file1, false)?;
        let mut document1_2 = api.add_document(&file1, false)?;
        let mut document2 = api.add_document(&file2, false)?;

        document2.data.insert("key".into(), "value".into());
        document1_2.data.insert("key".into(), "value".to_owned());
        document1_2.tags.insert("tag".into());
        api.modify(document1_2.clone())?;
        api.modify(document2.clone())?;

        assert_eq!(
            vec![document1_2],
            api.execute_search(&SearchQuery {
                tags: HashSet::from(["tag".into()]),
                data: HashMap::from([("key".into(), "value".to_owned())]),
                ..Default::default()
            })?
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_search_filter_custom() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        create_functional_pdf_extractor(&config)?;

        add_to_config(
            &config,
            r#"[search.custom_filters."later"]
            type = "builtin/time/after""#,
        )?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        let mut document1_1 = api.add_document(&file1, false)?;
        let mut document1_2 = api.add_document(&file1, false)?;
        let mut document2 = api.add_document(&file2, false)?;

        document1_1.data.insert(
            "added".into(),
            "2023-08-08T00:34:60.026490+09:30".to_owned(),
        );
        document1_2.data.insert(
            "added".into(),
            "2023-09-01T00:34:60.026490+09:30".to_owned(),
        );
        document2.data.insert(
            "added".into(),
            "2023-09-08T00:34:60.026490+09:30".to_owned(),
        );
        api.modify(document1_1.clone())?;
        api.modify(document1_2.clone())?;
        api.modify(document2.clone())?;

        assert_eq!(
            vec![document1_2, document2],
            api.execute_search(&SearchQuery {
                custom_filters: HashMap::from([("later".into(), Some("2023-09-01".to_owned()))]),
                ..Default::default()
            })?
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_search_filter_tag_custom_mix() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        create_functional_pdf_extractor(&config)?;

        add_to_config(
            &config,
            r#"[search.custom_filters."later"]
            type = "builtin/time/after""#,
        )?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        let mut document1_1 = api.add_document(&file1, false)?;
        let mut document1_2 = api.add_document(&file1, false)?;
        let mut document2 = api.add_document(&file2, false)?;

        document1_1.data.insert(
            "added".into(),
            "2023-08-08T00:34:60.026490+09:30".to_owned(),
        );
        document1_2.data.insert(
            "added".into(),
            "2023-09-01T00:34:60.026490+09:30".to_owned(),
        );
        document2.data.insert(
            "added".into(),
            "2023-09-08T00:34:60.026490+09:30".to_owned(),
        );
        document2.tags.insert("tag".into());
        api.modify(document1_1.clone())?;
        api.modify(document1_2.clone())?;
        api.modify(document2.clone())?;

        assert_eq!(
            vec![document2],
            api.execute_search(&SearchQuery {
                tags: HashSet::from(["tag".into()]),
                custom_filters: HashMap::from([("later".into(), Some("2023-09-01".to_owned()))]),
                ..Default::default()
            })?
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_search_filter_custom_multiple() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file1 = tmp.path().join("this-will-be-created1.pdf");
        let _f1 = File::create(&file1);
        let file2 = tmp.path().join("this-will-be-created2.pdf");
        let _f2 = File::create(&file2);

        create_functional_pdf_extractor(&config)?;

        add_to_config(
            &config,
            r#"[search.custom_filters."later"]
            type = "builtin/time/after"
            "#,
        )?;

        add_to_config(
            &config,
            r#"[search.custom_filters."before"]
            type = "builtin/time/before"
            "#,
        )?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        let mut document1_1 = api.add_document(&file1, false)?;
        let mut document1_2 = api.add_document(&file1, false)?;
        let mut document2 = api.add_document(&file2, false)?;

        document1_1.data.insert(
            "added".into(),
            "2023-08-08T00:34:60.026490+09:30".to_owned(),
        );
        document1_2.data.insert(
            "added".into(),
            "2023-09-01T00:34:60.026490+09:30".to_owned(),
        );
        document2.data.insert(
            "added".into(),
            "2023-09-08T00:34:60.026490+09:30".to_owned(),
        );
        api.modify(document1_1.clone())?;
        api.modify(document1_2.clone())?;
        api.modify(document2.clone())?;

        assert_eq!(
            vec![document1_2],
            api.execute_search(&SearchQuery {
                custom_filters: HashMap::from([
                    ("later".into(), Some("2023-09-01".to_owned())),
                    ("before".into(), Some("2023-09-04".to_owned()))
                ]),
                ..Default::default()
            })?
        );

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_extracts() -> Result<(), Box<dyn std::error::Error>> {
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file = tmp.path().join("this-will-be-created.pdf");
        let _f = File::create(&file);

        create_functional_pdf_extractor(&config)?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;
        api.add_document(&file, false)?;
        api.extract(&DocumentSelection::Latest)?;

        assert!(config
            .extracted_document_path(Slug::new("this-will-be-created"), "pdf")
            .exists());

        Ok(())
    }

    // Note: This test runs an external script.
    #[test]
    fn api_runs_script() -> Result<(), Box<dyn std::error::Error>> {
        env_logger::init();
        let tmp = tempdir()?;
        let config = InitializationConfig::new(&tmp);

        let file = tmp.path().join("this-will-be-created.pdf");
        let _f = File::create(&file);

        create_functional_pdf_extractor(&config)?;

        let creates = tmp.as_ref().join("creates");

        create_functional_script(&config, "script", &creates)?;

        let api = Arc::<arc_store_sqlite::SqliteStore, NoUserInteraction>::new(
            config.clone(),
            NoUserInteraction,
        )?;

        api.add_document(&file, false)?;
        api.script("script", Some(&DocumentSelection::Latest))?;

        assert!(creates.exists());

        Ok(())
    }

    fn create_functional_script<S: AsRef<str>, P: AsRef<Path>>(
        config: &InitializationConfig,
        name: S,
        path: P,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let name = name.as_ref();
        let path = path.as_ref();
        config.setup_directory_structure()?;

        add_to_config(
            config,
            &format!(
                r#"
                [mimes]

                [scripts."{}"]
                type = "{}"
                "#,
                name, name
            ),
        )?;

        let content = format!(
            r#"#!/bin/sh
            touch {}
            "#,
            path.display()
        );

        let mut script_file = File::create(config.script_path(Path::new(name)))?;
        script_file.write_all(content.as_bytes())?;
        let mut permissions = script_file.metadata()?.permissions();
        permissions.set_mode(0o777);
        script_file.set_permissions(permissions)?;

        Ok(())
    }
}
