use std::{
    fmt::Display,
    io::Write,
    path::{Path, PathBuf},
    process::{Command, Stdio},
};

use arc_core::{
    InitializationConfig, Tagger, TaggerError, TaggerOutput, ENV_CURRENT_DOCUMENT,
    ENV_CURRENT_TEXT, ENV_DOCUMENTS,
};

pub struct ExternalTagger {
    command: PathBuf,
    config: InitializationConfig,
}

impl ExternalTagger {
    pub fn new<P: AsRef<Path>>(config: InitializationConfig, command: P) -> Self {
        log::debug!(
            "Setting up an external tagger for the command: {}.",
            command.as_ref().display()
        );
        Self {
            config,
            command: command.as_ref().to_owned(),
        }
    }
}

#[derive(Debug)]
pub enum ExternalTaggerError {
    Io(std::io::Error),
    Serialize(serde_json::Error),
    OutputNoJson(String, serde_json::Error),
    TaggerExitedFailure(i32),
}

impl Display for ExternalTaggerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(e) => write!(f, "spawing an external tagger failed: {e}")?,
            Self::Serialize(e) => write!(f, "serializing the tagger input failed: {e}")?,
            Self::OutputNoJson(s, e) => write!(f, "the data that was returned by an external tagger was no JSON:\n{s}\nFailure due to: {e}")?,
            Self::TaggerExitedFailure(e) => write!(f, "the external tagger did not exit cleanly. Exit code: {e}")?,
        }
        Ok(())
    }
}

impl std::error::Error for ExternalTaggerError {}

impl From<std::io::Error> for ExternalTaggerError {
    fn from(value: std::io::Error) -> Self {
        Self::Io(value)
    }
}

impl From<ExternalTaggerError> for TaggerError {
    fn from(value: ExternalTaggerError) -> Self {
        TaggerError::TaggerError(Box::new(value))
    }
}

impl Tagger for ExternalTagger {
    fn tag(
        &self,
        document: arc_core::TaggerInput,
    ) -> Result<arc_core::TaggerOutput, arc_core::TaggerError> {
        log::debug!(
            "Calling external tagger command: {}.",
            self.command.display()
        );
        log::trace!("Building the command");
        let mut command = Command::new(self.config.tagger_path(&self.command));
        // Current dir
        log::trace!("Setting the current directory");
        command.current_dir(self.config.store_path());
        // Env variables
        log::trace!("Setting environmental variables.");

        let store_path = self.config.store_path();
        log::trace!("    - {} = {}", ENV_DOCUMENTS, store_path.display());
        command.env(ENV_DOCUMENTS, store_path);

        let current_document_path = self
            .config
            .document_path(document.id, &document.file_extension);
        log::trace!(
            "    - {} = {}",
            ENV_CURRENT_DOCUMENT,
            current_document_path.display()
        );
        command.env(ENV_CURRENT_DOCUMENT, current_document_path);
        let current_text_path = self.config.text_path(document.id);
        log::trace!(
            "    - {} = {}",
            ENV_CURRENT_TEXT,
            current_text_path.display()
        );
        command.env(ENV_CURRENT_TEXT, current_text_path);
        // Pipes
        log::trace!("Setting stdin and stdout pipes.");
        command.stdin(Stdio::piped());
        command.stdout(Stdio::piped());

        log::debug!("Running external command.");
        let mut child = command
            .spawn()
            .map_err(|e| TaggerError::TaggerError(Box::new(ExternalTaggerError::from(e))))?;
        // TODO: When is stdin not available?
        if let Some(stdin) = &mut child.stdin {
            stdin
                .write(
                    serde_json::to_string(&document)
                        .map_err(|e| {
                            TaggerError::TaggerError(Box::new(ExternalTaggerError::Serialize(e)))
                        })?
                        .as_bytes(),
                )
                .map_err(|e| TaggerError::TaggerError(Box::new(ExternalTaggerError::Io(e))))?;
        }
        let result = child
            .wait_with_output()
            .map_err(|e| TaggerError::TaggerError(Box::new(ExternalTaggerError::from(e))))?;

        if !result.status.success() {
            log::error!(
                "The external command did not exit cleanly: Exit code {}.",
                result.status.code().unwrap_or(-1)
            );
            return Err(ExternalTaggerError::TaggerExitedFailure(
                result.status.code().unwrap_or(-1),
            )
            .into());
        }

        log::trace!(
            "Parsing output of the command: {}",
            String::from_utf8_lossy(&result.stdout)
        );
        let result: TaggerOutput = serde_json::from_slice(&result.stdout).map_err(|e| {
            ExternalTaggerError::OutputNoJson(
                String::from_utf8_lossy(&result.stdout).to_string(),
                e,
            )
        })?;
        log::trace!("Parsed output of the command to: {:#?}.", result);

        Ok(result)
    }
}
