use std::collections::{HashMap, HashSet};

use arc_core::{Tagger, TaggerInput, TaggerOutput};

pub const DEFAULT_FORMAT: &str = "%+";

pub struct DateTimeTagger {}

impl DateTimeTagger {
    pub fn new() -> Self {
        DateTimeTagger {}
    }
}

impl Default for DateTimeTagger {
    fn default() -> Self {
        Self::new()
    }
}

impl Tagger for DateTimeTagger {
    fn tag(&self, document: TaggerInput) -> Result<TaggerOutput, arc_core::TaggerError> {
        let format = document
            .config
            .get("format")
            .cloned()
            .unwrap_or(DEFAULT_FORMAT.to_string());
        Ok(TaggerOutput {
            tags: HashSet::from([]),
            data: HashMap::from([(
                "added".into(),
                chrono::Local::now().format(&format).to_string(),
            )]),
        })
    }
}
