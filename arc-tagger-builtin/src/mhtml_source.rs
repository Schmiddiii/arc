use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
    fs::File,
    io::Read,
    str::FromStr,
};

use arc_core::{InitializationConfig, Tagger, TaggerError, TaggerInput, TaggerOutput};
use mail_parser::{HeaderValue, MessageParser};
use mime::Mime;

pub struct MhtmlSourceTagger {
    config: InitializationConfig,
}

impl MhtmlSourceTagger {
    pub fn new(config: InitializationConfig) -> Self {
        MhtmlSourceTagger { config }
    }
}

#[derive(Debug)]
pub enum MhtmlParserError {
    FailedDocumentParsing,
    NoSource,
}

#[derive(Debug)]
pub enum MhtmlSourceTaggerError {
    Io(std::io::Error),
    MhtmlParser(MhtmlParserError),
}

impl Display for MhtmlSourceTaggerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(e) => write!(f, "input/output error: {}", e)?,
            Self::MhtmlParser(MhtmlParserError::FailedDocumentParsing) => {
                write!(f, "failed to parse document as mhtml")?
            }
            Self::MhtmlParser(MhtmlParserError::NoSource) => {
                write!(f, "mhtml document has no source")?
            }
        }
        Ok(())
    }
}

impl std::error::Error for MhtmlSourceTaggerError {}

impl From<MhtmlSourceTaggerError> for TaggerError {
    fn from(value: MhtmlSourceTaggerError) -> Self {
        TaggerError::TaggerError(Box::new(value))
    }
}

impl Tagger for MhtmlSourceTagger {
    fn tag(&self, document: TaggerInput) -> Result<TaggerOutput, arc_core::TaggerError> {
        log::debug!(
            "Mthml source tagger tagging document with input: {:#?}.",
            document
        );

        if document.mime_type.type_() != "message" && document.mime_type.subtype() != "rfc822" {
            log::error!(
                "Called MHTML source tagger on a mime type that is not MHTML: {}",
                document.mime_type
            );
            return Err(TaggerError::InvalidMimeType(
                document.mime_type,
                Mime::from_str("message/rfc822").expect("parseable mime type"),
            ));
        }

        let current_document_path = self
            .config
            .document_path(document.id, document.file_extension);

        let mut file = File::open(current_document_path).map_err(MhtmlSourceTaggerError::Io)?;
        let mut bytes = Vec::new();

        file.read_to_end(&mut bytes)
            .map_err(MhtmlSourceTaggerError::Io)?;

        let message =
            MessageParser::default()
                .parse(&bytes)
                .ok_or(MhtmlSourceTaggerError::MhtmlParser(
                    MhtmlParserError::FailedDocumentParsing,
                ))?;

        let source = message.header("Snapshot-Content-Location").ok_or(
            MhtmlSourceTaggerError::MhtmlParser(MhtmlParserError::NoSource),
        )?;
        let HeaderValue::Text(source) = source else {
            return Err(MhtmlSourceTaggerError::MhtmlParser(MhtmlParserError::NoSource).into());
        };

        Ok(TaggerOutput {
            tags: HashSet::new(),
            data: HashMap::from([("source".into(), source.to_string())]),
        })
    }
}
