use std::collections::{HashMap, HashSet};

use arc_core::{Tagger, TaggerInput, TaggerOutput};

pub struct ExtensionTagger {}

impl ExtensionTagger {
    pub fn new() -> Self {
        ExtensionTagger {}
    }
}

impl Default for ExtensionTagger {
    fn default() -> Self {
        Self::new()
    }
}

impl Tagger for ExtensionTagger {
    fn tag(&self, document: TaggerInput) -> Result<TaggerOutput, arc_core::TaggerError> {
        Ok(TaggerOutput {
            tags: HashSet::from([document.file_extension.into()]),
            data: HashMap::new(),
        })
    }
}
