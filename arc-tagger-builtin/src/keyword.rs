use std::{
    collections::{HashMap, HashSet},
    fmt::Display,
};

use arc_core::{InitializationConfig, Slug, Tagger, TaggerError, TaggerInput, TaggerOutput};

pub type KeywordConfig = HashMap<String, String>;

pub struct KeywordTagger {
    config: InitializationConfig,
}

impl KeywordTagger {
    pub fn new(config: InitializationConfig) -> Self {
        KeywordTagger { config }
    }
}

#[derive(Debug)]
pub enum KeywordTaggerError {
    Io(std::io::Error),
}

impl Display for KeywordTaggerError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Io(e) => write!(f, "input/output error: {}", e)?,
        }
        Ok(())
    }
}

impl std::error::Error for KeywordTaggerError {}

impl From<KeywordTaggerError> for TaggerError {
    fn from(value: KeywordTaggerError) -> Self {
        TaggerError::TaggerError(Box::new(value))
    }
}

impl Tagger for KeywordTagger {
    fn tag(&self, document: TaggerInput) -> Result<TaggerOutput, arc_core::TaggerError> {
        log::debug!(
            "Keyword tagger tagging document with input: {:#?}.",
            document
        );
        let text_path = self.config.text_path(document.id);
        let text = std::fs::read_to_string(text_path).map_err(KeywordTaggerError::Io)?;

        let keywords = document.config;

        Ok(TaggerOutput {
            tags: tags(text, keywords),
            data: HashMap::new(),
        })
    }
}

// TODO: Not really efficient.
fn tags(text: String, config: KeywordConfig) -> HashSet<Slug> {
    let mut tags = HashSet::new();
    for (k, v) in config {
        if text.contains(&k) {
            tags.insert(v.into());
        }
    }

    tags
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn tags_simple() {
        assert_eq!(
            HashSet::from(["tag1".into()]),
            tags(
                "Hello World".to_owned(),
                HashMap::from([("Hello".into(), "tag1".to_string())])
            )
        )
    }

    #[test]
    fn tags_empty() {
        assert_eq!(
            HashSet::new(),
            tags(
                "Hi World".to_owned(),
                HashMap::from([("Hello".into(), "tag1".into())])
            )
        )
    }

    #[test]
    fn tags_multiple_occurences() {
        assert_eq!(
            HashSet::from(["tag1".into()]),
            tags(
                "Hello Hello World Hello".to_owned(),
                HashMap::from([("Hello".into(), "tag1".into())])
            )
        )
    }

    #[test]
    fn tags_multiple_tags() {
        assert_eq!(
            HashSet::from(["tag1".into(), "tag2".into()]),
            tags(
                "Hello World".to_owned(),
                HashMap::from([
                    ("Hello".to_string(), "tag1".to_string()),
                    ("World".to_string(), "tag2".to_string())
                ])
            )
        )
    }
}
