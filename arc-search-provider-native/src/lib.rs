use std::{
    fs::File,
    io::{BufRead, BufReader},
};

use arc_core::{InitializationConfig, SearchProvider};

pub struct NativeSearchProvider(InitializationConfig);

impl NativeSearchProvider {
    // TODO: Allow configuration (e.g. case-sensitive).
    pub fn new(config: InitializationConfig) -> Self {
        Self(config)
    }
}

impl SearchProvider for NativeSearchProvider {
    fn contains(&self, document: &arc_core::Document, text: &str) -> bool {
        let text = text.to_lowercase();
        let text_file = self.0.text_path(document.id);
        let Ok(file) = File::open(&text_file) else {
            return false;
        };
        let reader = BufReader::new(file);

        for line in reader.lines() {
            if line.is_ok_and(|l| l.to_lowercase().contains(&text)) {
                return true;
            }
        }

        false
    }
}

#[cfg(test)]
mod test {
    use std::{error::Error, io::Write};

    use arc_core::Document;

    use super::*;

    #[test]
    fn search_contains() -> Result<(), Box<dyn Error>> {
        let tmp = tempfile::tempdir()?;
        let config = InitializationConfig::new(&tmp);
        config.setup_directory_structure()?;
        let path = config.text_path(1);
        let mut file = File::create(path)?;
        write!(file, "This contains something")?;

        let search = NativeSearchProvider(config);

        assert!(search.contains(
            &Document {
                id: 1,
                slug: "".into(),
                file_extension: "".into(),
                mime_type: mime::APPLICATION_PDF,
                title: Default::default(),
                tags: Default::default(),
                data: Default::default()
            },
            "something"
        ));
        Ok(())
    }

    #[test]
    fn search_not_contains() -> Result<(), Box<dyn Error>> {
        let tmp = tempfile::tempdir()?;
        let config = InitializationConfig::new(&tmp);
        config.setup_directory_structure()?;
        let path = config.text_path(1);
        let mut file = File::create(path)?;
        write!(file, "This contains nothing")?;

        let search = NativeSearchProvider(config);

        assert!(!search.contains(
            &Document {
                id: 1,
                slug: "".into(),
                file_extension: "".into(),
                mime_type: mime::APPLICATION_PDF,
                title: Default::default(),
                tags: Default::default(),
                data: Default::default()
            },
            "something"
        ));
        Ok(())
    }

    #[test]
    fn search_contains_case_insensitive() -> Result<(), Box<dyn Error>> {
        let tmp = tempfile::tempdir()?;
        let config = InitializationConfig::new(&tmp);
        config.setup_directory_structure()?;
        let path = config.text_path(1);
        let mut file = File::create(path)?;
        write!(file, "This contains SoMeThInG")?;

        let search = NativeSearchProvider(config);

        assert!(search.contains(
            &Document {
                id: 1,
                slug: "".into(),
                file_extension: "".into(),
                mime_type: mime::APPLICATION_PDF,
                title: Default::default(),
                tags: Default::default(),
                data: Default::default()
            },
            "sOmEtHiNg"
        ));
        Ok(())
    }

    #[test]
    fn search_not_contains_no_document() -> Result<(), Box<dyn Error>> {
        let tmp = tempfile::tempdir()?;
        let config = InitializationConfig::new(&tmp);
        config.setup_directory_structure()?;
        let search = NativeSearchProvider(config);

        assert!(!search.contains(
            &Document {
                id: 1,
                slug: "".into(),
                file_extension: "".into(),
                mime_type: mime::APPLICATION_PDF,
                title: Default::default(),
                tags: Default::default(),
                data: Default::default()
            },
            "sOmEtHiNg"
        ));
        Ok(())
    }
}
